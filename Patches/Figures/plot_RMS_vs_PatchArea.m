%% RMS Error vs. Patch Area 
% 
% plot the root mean squared error of voltage at contact locations between the
% patch model and both centroid models (simple and best) as a function of
% patch area

% **********************************************************************
% 10/12/20
% Grace Dessert
% **********************************************************************


semilog_y = 1; % 1 = semilog plot,  else = not semilog
plot_boxplot = 0; % 1 = box plot, else = no boxplot
centroid_model = 2; % 1 = simple model,  2 = best model
resolution = 1;
all_res = 0; %  1 == plot all resolutions together


path = ('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/');
pathplot = '/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PublishAll/';

% LH 
% load patch contact voltages
load([path 'PatchContactVoltages_highRes.mat'],'patch_contact_voltages')
% load centroid true contact voltages
load([path 'CentroidModelTrueDirContactVoltages_highRes.mat'],'CentroidModelTrueDirContactVoltages'); 
% load centroid best dir contact voltages
load([path 'CentroidModelBestDirContactVoltages.mat'],'bestC_CVs'); 
% load patch areas and distances to electrode
load([path 'Patches_LH_all.mat'],'patchesLH')
patch_contact_voltages_LH = cell(3,1);
CentroidModelTrueDirContactVoltages_LH = cell(3,1);
bestC_CVs_LH = cell(3,1);
patch_areas_LH = cell(3,1);
dist_to_closest_contact_LH = cell(3,1);
for r = 1:3
    patch_contact_voltages_LH{r} = patch_contact_voltages{r};
    CentroidModelTrueDirContactVoltages_LH{r} = CentroidModelTrueDirContactVoltages{r};
    bestC_CVs_LH{r} = bestC_CVs{r};
    patch_areas_LH{r} = patchesLH{r}.areas;
    dist_to_closest_contact_LH{r} = patchesLH{r}.minDtoContact;
end

% RH load 
% load patch contact voltages
load([path 'PatchContactVoltagesR_highRes.mat'],'patch_contact_voltages')
% load centroid true contact voltages
load([path 'CentroidModelTrueDirContactVoltagesR_highRes.mat'],'CentroidModelTrueDirContactVoltages'); 
% load centroid best dir contact voltages
load([path 'CentroidModelBestDirContactVoltagesR.mat'],'bestC_CVs'); 
% load patch areas and distances to electrode
load([path 'Patches_RH_all.mat'],'patchesRH')
patch_contact_voltages_RH = cell(3,1);
CentroidModelTrueDirContactVoltages_RH = cell(3,1);
bestC_CVs_RH = cell(3,1);
patch_areas_RH = cell(3,1);
dist_to_closest_contact_RH = cell(3,1);
for r = 1:3
    patch_contact_voltages_RH{r} = patch_contact_voltages{r};
    CentroidModelTrueDirContactVoltages_RH{r} = CentroidModelTrueDirContactVoltages{r};
    bestC_CVs_RH{r} = bestC_CVs{r};
    patch_areas_RH{r} = patchesRH{r}.areas;
    dist_to_closest_contact_RH{r} = patchesRH{r}.minDtoContact;
end

% combine LH and RH data
patch_areas = cell(3,1);
dist_to_closest_contact = cell(3,1);
for r  = 1:3
patch_contact_voltages{r} = [patch_contact_voltages_LH{r},patch_contact_voltages_RH{r}];
CentroidModelTrueDirContactVoltages{r} = [CentroidModelTrueDirContactVoltages_LH{r},CentroidModelTrueDirContactVoltages_RH{r}];
bestC_CVs{r} = [bestC_CVs_LH{r},bestC_CVs_RH{r}];
patch_areas{r} = [patch_areas_LH{r}',patch_areas_RH{r}'];
dist_to_closest_contact{r} = [dist_to_closest_contact_LH{r}',dist_to_closest_contact_RH{r}'];
end

rms = cell(3,1);
for r = 1:3
    % RMS (root mean squared) error is 
    rms{r} = zeros(1,800);
    
    for patchI = 1:800
    patch_voltages = patch_contact_voltages{r}(:,patchI);
    if centroid_model == 1
    raw_centroid_CVs = CentroidModelTrueDirContactVoltages{r}(:,patchI);
    elseif centroid_model == 2
        raw_centroid_CVs = bestC_CVs{r}(:,patchI);
    end
    % get best dipole moment for centroid model (slope of linear regression)
    patchI;
    slope = raw_centroid_CVs \ patch_voltages;
    scaled_best_centroid_CVs = raw_centroid_CVs * slope;
    %plot(scaled_best_centroid_CVs,patch_voltages,'.')
    %pause(0.1)
    rms{r}(patchI) = sqrt(mean((scaled_best_centroid_CVs-patch_voltages).^2));
    end
end


% largest potential difference among corresponding electrodes
figure(2)
clf 
if all_res == 1
    colors = parula(4); % use first 3
    for r = 1:3
        for pLoc=1:40
            indices = [(pLoc-1)*20+1:pLoc*20];
            if semilog_y == 1
                semilogy(patch_areas{r}(indices)/100,rms{r}(indices),'.-','Color',colors(r,:),'DisplayName',['resolution: ' num2str(r)])
            else
                plot(patch_areas{r}(indices)/100,rms{r}(indices),'.-','Color',colors(r,:),'DisplayName',['resolution: ' num2str(r)])
            end
            hold on
            %plot(indices/100,max_error(indices),'.-')
        end
    end
    if semilog_y == 1
        p = plot([0,10],[50,50],'k-','LineWidth',1.1,'DisplayName','50�V');
        legend(p,'Location','northwest')

        q = plot(patch_areas{1}(indices)/100,rms{1}(indices),'.-','Color',colors(1,:),'DisplayName',['full resolution']);
        s = plot(patch_areas{2}(indices)/100,rms{2}(indices),'.-','Color',colors(2,:),'DisplayName',['145k resolution']);
        t = plot(patch_areas{3}(indices)/100,rms{3}(indices),'.-','Color',colors(3,:),'DisplayName',['20k resolution']);

        legend([p q s t])
        legend('Location','Northwest')
    end
    if centroid_model == 1
        title({'RMS Error of Contact Voltages for all patch resolutions','Centroid Model: Simple'})
    elseif centroid_model == 2
        title({'RMS Error of Contact Voltages for all patch resolutions','Centroid Model: Best'})
    end
    xlabel('Area of Patch (cm^2)')
    ylabel('Root Mean Squared Error (�V)')
    
else
    if plot_boxplot == 1
        hold on
        areas = [0.1,[0.5:0.5:9.5]]*100;
        rms_bp = zeros(40,20);
        for pArea=1:20 
            indices = [0:1:39]*20+pArea;
            rms_bp(:,pArea) = rms{resolution}(indices);
        end
        boxplot(rms_bp,'Labels',{'0.1','0.5','1','1.5','2','2.5','3','3.5','4','4.5','5','5.5','6','6.5','7','7.5','8','8.5','9','9.5'});
        if semilog_y == 1
            set(gca,'yscale','log')
            p = plot([0,22],[50,50],'k-','LineWidth',1.1,'DisplayName','50�V');
        legend(p,'Location','northwest')
        end
    else
        for pLoc=10:40
            indices = [(pLoc-1)*20+1:pLoc*20];
            if semilog_y == 1
                semilogy(patch_areas{resolution}(indices)/100,rms{resolution}(indices),'.-')
            else
                plot(patch_areas{resolution}(indices)/100,rms{resolution}(indices),'.-')
            end
            hold on
            %plot(indices/100,max_error(indices),'.-')
        end
        if semilog_y == 1
            p = plot([0,10],[50,50],'k-','LineWidth',1.1,'DisplayName','50�V');
            legend(p,'Location','northwest')
        end
    end
    if centroid_model == 1
        title({['RMS Error of Contact Voltages for resolution ' num2str(resolution)],'Centroid Model: Simple'})
    elseif centroid_model == 2
        title({['RMS Error of Contact Voltages for resolution ' num2str(resolution)],'Centroid Model: Best'})
    end
    xlabel('Area of Patch (cm^2)')
    ylabel('Root Mean Squared Error (�V)')
end


if all_res == 1
    if centroid_model == 1 % simple model
        if semilog_y == 1 % semilog
            fileName=[pathplot 'Figures/RMS_Semilog_Simple_allRes.pdf'];
        else
            fileName=[pathplot 'Figures/RMS_Simple_allRes.pdf'];
        end
    elseif centroid_model == 2 % best model
        if semilog_y == 1 % semilog
            fileName=[pathplot 'Figures/RMS_Semilog_Best_allRes.pdf'];
        else
            fileName=[pathplot 'Figures/RMS_Best_allRes.pdf'];
        end
    end
else
    if plot_boxplot ~= 1
        if centroid_model == 1 % simple model
            if semilog_y == 1 % semilog
                fileName=[pathplot 'Figures/RMS_Semilog_Simple.pdf'];
            else
                fileName=[pathplot 'Figures/RMS_Simple.pdf'];
            end
        elseif centroid_model == 2 % best model
            if semilog_y == 1 % semilog
                fileName=[pathplot 'Figures/RMS_Semilog_Best.pdf'];
            else
                fileName=[pathplot 'Figures/RMS_Best.pdf'];
            end
        end
    else
        if centroid_model == 1 % simple model
            if semilog_y == 1 % semilog
                fileName=[pathplot 'Figures/RMS_BP_Semilog_Simple.pdf'];
            else
                fileName=[pathplot 'Figures/RMS_BP_Simple.pdf'];
            end
        elseif centroid_model == 2 % best model
            if semilog_y == 1 % semilog
                fileName=[pathplot 'Figures/RMS_BP_Semilog_Best.pdf'];
            else
                fileName=[pathplot 'Figures/RMS_BP_Best.pdf'];
            end
        end
    end
end
print(gcf,fileName,'-bestfit','-dpdf');
