%% Test Practical Linearity - plot max voltage difference between centroid LC and true models as a function of Patch Area

% **********************************************************************
% 10/12/20
% Grace Dessert
% **********************************************************************

% given the contact voltages of orthogonal basis dipole sources,
% can we use linear combinations of these to find the voltage of any
% centroid dipole source?

% **********************************************************************

semilog_y = 1; % 1 = semilog plot,  else = not semilog
r = 1;
all_res = 1; %  1 == plot all resolutions together

path = '/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/';
pathplot = '/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PublishAll/';
% load data
% LH
load([path 'Patches_LH_all.mat'],'patchesLH')
load([path 'max_error_centroid_LCtrueDir_contactVoltages_all.mat'],'max_error')
patch_areas_LH = patchesLH{r}.areas;
max_error_LH = max_error;    

load([path 'best_dipole_moment_for_simple_centroid_model_all.mat'],'best_dipole_moment_for_simple_centroid_model')
dipoleMoment_scale_LH = best_dipole_moment_for_simple_centroid_model;


% RH
load([path 'Patches_RH_all.mat'],'patchesRH')
patch_areas_RH = patchesRH{r}.areas;

load([path 'max_error_centroid_LCtrueDir_contactVoltages_RH_all.mat'],'max_error')
max_error_RH = max_error;

load([path 'best_dipole_moment_for_simple_centroid_modelRH_all.mat'],'best_dipole_moment_for_simple_centroid_model')
dipoleMoment_scale_RH = best_dipole_moment_for_simple_centroid_model;

if all_res == 1
    patch_areas = cell(3,1);
    dipoleMoment_scale = cell(3,1);
    for r = 1:3
        patch_areas{r} = [patchesLH{r}.areas,patch_areas_RH];
        max_error{r} = [max_error_LH{r},max_error_RH{r}];
        % both hemispheres
        dipoleMoment_scale{r} = zeros(800,1);
        dipoleMoment_scale{r}(1:400) = dipoleMoment_scale_LH{r};
        dipoleMoment_scale{r}(401:800) = dipoleMoment_scale_RH{r};
    end
else
    max_error = [max_error_LH{r},max_error_RH{r}];
    patch_areas = [patch_areas_LH,patch_areas_RH];
    % both hemispheres
    dipoleMoment_scale = zeros(800,1);
    dipoleMoment_scale(1:400) = best_dipole_moment_for_simple_centroid_model{r};
    dipoleMoment_scale(401:800) = best_dipole_moment_for_simple_centroid_model{r};
end

% plot data
figure(1)
clf
if all_res == 1
    colors = parula(4); % use first 3
    for r = 1:3
        for pLoc = 1:40 % for each patch location, plot 20 patches
            indices = [(pLoc-1)*20+1:pLoc*20];
            if semilog_y == 1 %semilog plot
                semilogy(patch_areas{r}(indices)./100,max_error{r}(indices),'.-','Color',colors(r,:),'DisplayName',['resolution: ' num2str(r)])
                ylim([0,100])
            else %.*abs(best_dipole_moment_for_simple_centroid_model(indices))
                plot(patch_areas{r}(indices)./100,max_error{r}(indices),'.-','Color',colors(r,:),'DisplayName',['resolution: ' num2str(r)])
                ylim([0,65])
            end
            hold on
        end
    end
    q = plot(patch_areas{1}(indices)./100,max_error{1}(indices),'.-','Color',colors(1,:),'DisplayName',['full resolution']);
    s = plot(patch_areas{2}(indices)./100,max_error{2}(indices),'.-','Color',colors(2,:),'DisplayName',['145k resolution']);
    t = plot(patch_areas{3}(indices)./100,max_error{3}(indices),'.-','Color',colors(3,:),'DisplayName',['20k resolution']);
    p = plot([0,10],[15,15],'k-','LineWidth',1.1,'DisplayName','15 �V');
    hold off
    legend([p q s t])
    legend('Location','Northwest')

    title({'Max Voltage Difference for all patch resolutions','between Linear Combination and True Direction Centroid Dipole'});
    xlabel('Patch Area (cm^2)')
    ylabel('Max Voltage Difference (�V)')
else
    for pLoc = 1:40 % for each patch location, plot 20 patches
    indices = [(pLoc-1)*20+1:pLoc*20]
    if semilog_y == 1 %semilog plot
        semilogy(patch_areas(indices)./100,max_error(indices),'.-')
        ylim([0,100])
    else %.*abs(best_dipole_moment_for_simple_centroid_model(indices))
        plot(patch_areas(indices)./100,max_error(indices),'.-')
        ylim([0,55])
    end
    hold on
    end
    p = plot([0,10],[15,15],'k-','LineWidth',1.1,'DisplayName','15 �V');
    legend(p)

    title({['Max Voltage Difference for patch resolution ' num2str(r)],'between Linear Combination and True Direction Centroid Dipole'});
    xlabel('Patch Area (cm^2)')
    ylabel('Max Voltage Difference (�V)')
end


if all_res == 1
    if semilog_y == 1
    fileName=[pathplot 'Figures/TestPracticalLinearity_AllRes_semilog_MaxVoltageDiff_vs_PatchArea.pdf'];
    else
        fileName=[pathplot 'Figures/TestPracticalLinearity_AllRes_MaxVoltageDiff_vs_PatchArea.pdf'];
    end
    print(gcf,fileName,'-bestfit','-dpdf');
else
    if semilog_y == 1
    fileName=[pathplot 'Figures/TestPracticalLinearity_semilog_MaxVoltageDiff_vs_PatchArea.pdf'];
    else
        fileName=[pathplot 'Figures/TestPracticalLinearity_MaxVoltageDiff_vs_PatchArea.pdf'];
    end
    print(gcf,fileName,'-bestfit','-dpdf');
end


