%% concatenate centroid model data (forward solutions and contact voltage matrices)

% ************************************************************************
% 10/12/20
% Grace Dessert
% ************************************************************************

% for each of three patch resolutions (20k, 145k, full)
% concatenate all contact voltage fields from 3200 dipoles into 16 files
% and concatenate all forward solutions from 3200 dipoles into 16 files.
% for each patch, there are four centroid dipoles (true direction, x-dir,
% y-dir, and z-dir). 

% input:    
%           Centroids/FS/HeadDipoleVoltage(R)_$.mat - scirunmatrix - in �V
%               466779x1 double
%               forward solutions for centroid dipoles. $=1:4800 for
%               each hemisphere. (first 1600 are for 145k res, second 1600 for
%               20k res, third 1600 for full res)
%           Centroids/ContactVoltage/HeadDipoleVoltage_ContactLocs_$.mat - scirunmatrix - in �V
%               112x1 double
%               contact voltages for centroid dipoles. $=1:4800 for each
%               hemisphere. (first 1600 are for 145k res, second 1600 for
%               20k res, third 1600 for full res)
%           DipoleFaces/FS/HeadDipoleVoltage(R)_$.mat - scirunmatrix - in �V
%               466779x1 double
%               forward solutions for elementary face surface dipoles.
%           DipoleFaces/ContactVoltage/HeadDipoleVoltage_ContactLocs_$.mat - scirunmatrix - in �V
%               112x1 double
%               contact voltages for elementary face surface dipoles. 
%               

% output:
%           CentroidModelTrueDirContactVoltages  -  for LH and RH  -  in �V
%               3x1 cell array of 112x400 double
%               contact voltages of true direction centroid dipole for all
%               800 patches. centroid dipole is 20nAm current dipole in
%               direction of linear combination of corresponding patch face
%               normal vectors (weighted by each face's area).
%           CentroidModelXContactVoltages, CentroidModelYContactVoltages, CentroidModelZContactVoltages - for LH and RH
%               3x1 cell array of 112x400 double 
%               contact voltages of three orthogonal basis centroid dipoles
%               for all 800 patches. centroid dipoles are 20nAm current 
%               dipoles in the (1,0,0), (0,1,0), and (0,0,1) directions. 

% ************************************************************************


%% concatenate centroid model data
% concatenate CentroidModel data, higher resolution patches
%LH patches. 400.
CentroidModelTrueDirContactVoltages= cell(3,1);
CentroidModelXContactVoltages= cell(3,1);
CentroidModelYContactVoltages= cell(3,1);
CentroidModelZContactVoltages= cell(3,1);

CentroidModelTrueDirForwardSolutions= cell(3,1);
CentroidModelXForwardSolutions= cell(3,1);
CentroidModelYForwardSolutions= cell(3,1);
CentroidModelZForwardSolutions= cell(3,1);

rs = [2,1,3];
for rInd = 1:3
    r = rs(rInd); % iterate r = 2, 1, then 3
    %   but use rInd to get indices of files
    %   this is because r==2 corresponds to first 1600 file indices
    
    CentroidModelTrueDirContactVoltages{r} = zeros(112,400);
    CentroidModelXContactVoltages{r} = zeros(112,400);
    CentroidModelYContactVoltages{r} = zeros(112,400);
    CentroidModelZContactVoltages{r} = zeros(112,400);

    CentroidModelTrueDirForwardSolutions{r} = zeros(466779,400);
    CentroidModelXForwardSolutions{r} = zeros(466779,400);
    CentroidModelYForwardSolutions{r} = zeros(466779,400);
    CentroidModelZForwardSolutions{r} = zeros(466779,400);
    missing = [];

for p = 1:400
    patchI = (rInd-1)*400+p;
    
    % TrueDir FS
    ind = (patchI-1)*4+1;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltage_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltage_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelTrueDirForwardSolutions{r}(:,p) = scirunmatrix;
    else % File does not exist.
        ind
        missing = [missing,ind];
    end
    
    % X FS
    ind = (patchI-1)*4+2;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltage_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltage_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelXForwardSolutions{r}(:,p) = scirunmatrix;
    else % File does not exist.
        ind
        missing = [missing,ind];
    end
    
    % Y FS
    ind = (patchI-1)*4+3;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltage_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltage_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelYForwardSolutions{r}(:,p) = scirunmatrix;
    else % File does not exist.
       ind
        missing = [missing,ind];
    end
    
    % Z FS
    ind = (patchI-1)*4+4;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltage_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltage_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelZForwardSolutions{r}(:,p) = scirunmatrix;
    else % File does not exist.
        ind
        missing = [missing,ind];
    end
    
    
    
    % TrueDir Contact Voltages
    ind = (patchI-1)*4+1;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltage_ContactLocs_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltage_ContactLocs_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelTrueDirContactVoltages{r}(:,p) = scirunmatrix;
    else % File does not exist.
        x = 1;
    end
    
    % X Contact Voltages
    ind = (patchI-1)*4+2;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltage_ContactLocs_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltage_ContactLocs_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelXContactVoltages{r}(:,p) = scirunmatrix;
    else % File does not exist.
        x = 1;
    end
    
    
    % Y Contact Voltages
    ind = (patchI-1)*4+3;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltage_ContactLocs_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltage_ContactLocs_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelYContactVoltages{r}(:,p) = scirunmatrix;
    else % File does not exist.
        x = 1;
    end
    
    
    % Z Contact Voltages
    ind = (patchI-1)*4+4;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltage_ContactLocs_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltage_ContactLocs_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelZContactVoltages{r}(:,p) = scirunmatrix;
    else % File does not exist.
        x = 1;
    end
end
    
    % save select inds for visualization

    disp(r)
    
    voltage_centroidTrueDirFSs_vis{r} = zeros(466779,9);
    voltage_centroidXFSs_vis{r} = zeros(466779,9);
    voltage_centroidYFSs_vis{r} = zeros(466779,9);
    voltage_centroidZFSs_vis{r} = zeros(466779,9);

    inds = [1,25,50,75,100,125,150,175,200];

    for i=1:9
        inds = [1,25,50,75,100,125,150,175,200];
        p = inds(i);
        voltage_centroidTrueDirFSs_vis{r}(:,i) = CentroidModelTrueDirForwardSolutions{r}(:,p);
        voltage_centroidXFSs_vis{r}(:,i) = CentroidModelXForwardSolutions{r}(:,p);
        voltage_centroidYFSs_vis{r}(:,i) = CentroidModelYForwardSolutions{r}(:,p);
        voltage_centroidZFSs_vis{r}(:,i) = CentroidModelZForwardSolutions{r}(:,p);
    end

    while length(missing)>=30
       array = join(string(missing(1:30)),',')
       missing = missing(31:end);
    end
    array = join(string(missing),',')

end


save('HigherRes/CentroidModelTrueDirContactVoltages_highRes.mat','CentroidModelTrueDirContactVoltages'); 
save('HigherRes/CentroidModelXContactVoltages_highRes.mat','CentroidModelXContactVoltages'); 
save('HigherRes/CentroidModelYContactVoltages_highRes.mat','CentroidModelYContactVoltages'); 
save('HigherRes/CentroidModelZContactVoltages_highRes.mat','CentroidModelZContactVoltages'); 

% save('HigherRes/CentroidModelTrueDirForwardSolutions_highRes.mat','CentroidModelTrueDirForwardSolutions'); 
% save('HigherRes/CentroidModelXForwardSolutions_highRes.mat','CentroidModelXForwardSolutions'); 
% save('HigherRes/CentroidModelYForwardSolutions_highRes.mat','CentroidModelYForwardSolutions'); 
% save('HigherRes/CentroidModelZForwardSolutions_highRes.mat','CentroidModelZForwardSolutions'); 


save('HigherRes/Vis/CentroidTrueDirForwardSolutions_Inds1,25,50,75,100,125,150,175,200_forVis.mat','voltage_centroidTrueDirFSs_vis'); 
save('HigherRes/Vis/CentroidXForwardSolutions_Inds1,25,50,75,100,125,150,175,200_forVis.mat','voltage_centroidXFSs_vis'); 
save('HigherRes/Vis/CentroidYForwardSolutions_Inds1,25,50,75,100,125,150,175,200_forVis.mat','voltage_centroidYFSs_vis'); 
save('HigherRes/Vis/CentroidZForwardSolutions_Inds1,25,50,75,100,125,150,175,200_forVis.mat','voltage_centroidZFSs_vis');   


%% RH

%RH patches. 400.
CentroidModelTrueDirContactVoltages= cell(3,1);
CentroidModelXContactVoltages= cell(3,1);
CentroidModelYContactVoltages= cell(3,1);
CentroidModelZContactVoltages= cell(3,1);

CentroidModelTrueDirForwardSolutions= cell(3,1);
CentroidModelXForwardSolutions= cell(3,1);
CentroidModelYForwardSolutions= cell(3,1);
CentroidModelZForwardSolutions= cell(3,1);

rs = [2,1,3];
for rInd = 1:3
    r = rs(rInd); % iterate r = 2, 1, then 3
    %   but use rInd to get indices of files
    %   this is because r==2 corresponds to first 1600 file indices
    
    CentroidModelTrueDirContactVoltages{r} = zeros(112,400);
    CentroidModelXContactVoltages{r} = zeros(112,400);
    CentroidModelYContactVoltages{r} = zeros(112,400);
    CentroidModelZContactVoltages{r} = zeros(112,400);

    CentroidModelTrueDirForwardSolutions{r} = zeros(466779,400);
    CentroidModelXForwardSolutions{r} = zeros(466779,400);
    CentroidModelYForwardSolutions{r} = zeros(466779,400);
    CentroidModelZForwardSolutions{r} = zeros(466779,400);
    missing = [];

for p = 1:400
    patchI = (rInd-1)*400+p;
    
    % TrueDir FS
    ind = (patchI-1)*4+1;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltageR_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltageR_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelTrueDirForwardSolutions{r}(:,p) = scirunmatrix;
    else % File does not exist.
        x = 1;
    end
    
    % X FS
    ind = (patchI-1)*4+2;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltageR_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltageR_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelXForwardSolutions{r}(:,p) = scirunmatrix;
    else % File does not exist.
        x = 1;
    end
    
    % Y FS
    ind = (patchI-1)*4+3;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltageR_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltageR_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelYForwardSolutions{r}(:,p) = scirunmatrix;
    else % File does not exist.
        x = 1;
    end
    
    % Z FS
    ind = (patchI-1)*4+4;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltageR_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltageR_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelZForwardSolutions{r}(:,p) = scirunmatrix;
    else % File does not exist.
        x = 1;
    end
    
    
    
    % TrueDir Contact Voltages
    ind = (patchI-1)*4+1;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltageR_ContactLocs_' num2str(ind) '.mat']);
    if isfile(name) % file exists
        load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltageR_ContactLocs_' num2str(ind) '.mat'],'scirunmatrix'); 
        CentroidModelTrueDirContactVoltages{r}(:,p) = scirunmatrix;
    else % File does not exist.
        ind
        missing = [missing,ind];
    end
    
    % X Contact Voltages
    ind = (patchI-1)*4+2;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltageR_ContactLocs_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltageR_ContactLocs_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelXContactVoltages{r}(:,p) = scirunmatrix;
    else % File does not exist.
        ind
        missing = [missing,ind];
    end
    
    
    % Y Contact Voltages
    ind = (patchI-1)*4+3;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltageR_ContactLocs_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltageR_ContactLocs_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelYContactVoltages{r}(:,p) = scirunmatrix;
    else % File does not exist.
        ind
        missing = [missing,ind];
    end
    
    
    % Z Contact Voltages
    ind = (patchI-1)*4+4;
	name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltageR_ContactLocs_' num2str(ind) '.mat']);
    if isfile(name) % file exists
    load(['/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltageR_ContactLocs_' num2str(ind) '.mat'],'scirunmatrix'); 
    CentroidModelZContactVoltages{r}(:,p) = scirunmatrix;
    else % File does not exist.
        ind
        missing = [missing,ind];
    end
    
end

    % save select inds for visualization

    voltage_centroidTrueDirFSs_vis{r} = zeros(466779,9);
    voltage_centroidXFSs_vis{r} = zeros(466779,9);
    voltage_centroidYFSs_vis{r} = zeros(466779,9);
    voltage_centroidZFSs_vis{r} = zeros(466779,9);

    inds = [1,25,50,75,100,125,150,175,200];

    for i=1:9
        inds = [1,25,50,75,100,125,150,175,200];
        p = inds(i);
        voltage_centroidTrueDirFSs_vis{r}(:,i) = CentroidModelTrueDirForwardSolutions{r}(:,p);
        voltage_centroidXFSs_vis{r}(:,i) = CentroidModelXForwardSolutions{r}(:,p);
        voltage_centroidYFSs_vis{r}(:,i) = CentroidModelYForwardSolutions{r}(:,p);
        voltage_centroidZFSs_vis{r}(:,i) = CentroidModelZForwardSolutions{r}(:,p);
    end

    while length(missing)>=30
       array = join(string(missing(1:30)),',')
       missing = missing(31:end);
    end
    array = join(string(missing),',')

end


save('HigherRes/CentroidModelTrueDirContactVoltagesR_highRes.mat','CentroidModelTrueDirContactVoltages'); 
save('HigherRes/CentroidModelXContactVoltagesR_highRes.mat','CentroidModelXContactVoltages'); 
save('HigherRes/CentroidModelYContactVoltagesR_highRes.mat','CentroidModelYContactVoltages'); 
save('HigherRes/CentroidModelZContactVoltagesR_highRes.mat','CentroidModelZContactVoltages'); 

% save('HigherRes/CentroidModelTrueDirForwardSolutionsR_highRes.mat','CentroidModelTrueDirForwardSolutions'); 
% save('HigherRes/CentroidModelXForwardSolutionsR_highRes.mat','CentroidModelXForwardSolutions'); 
% save('HigherRes/CentroidModelYForwardSolutionsR_highRes.mat','CentroidModelYForwardSolutions'); 
% save('HigherRes/CentroidModelZForwardSolutionsR_highRes.mat','CentroidModelZForwardSolutions'); 

save('HigherRes/Vis/CentroidTrueDirForwardSolutionsR_Inds1,25,50,75,100,125,150,175,200_forVis.mat','voltage_centroidTrueDirFSs_vis'); 
save('HigherRes/Vis/CentroidXForwardSolutionsR_Inds1,25,50,75,100,125,150,175,200_forVis.mat','voltage_centroidXFSs_vis'); 
save('HigherRes/Vis/CentroidYForwardSolutionsR_Inds1,25,50,75,100,125,150,175,200_forVis.mat','voltage_centroidYFSs_vis'); 
save('HigherRes/Vis/CentroidZForwardSolutionsR_Inds1,25,50,75,100,125,150,175,200_forVis.mat','voltage_centroidZFSs_vis');   



