%% Create LH python scripts to create basis of ~40,000 forward solutions

% Single-Ended and Differntial Recording 
% Create python scripts make SCIRun networks for each dipole.
% Each script, to be run on the Duke Computing Cluster, creates a SCIRun 
% network to run a unit dipole current source on our FEM. Each dipole current source is located at the 
% centroid of one face on the cortex surface, oriented orthogonally. The SCIRun networks output 
% a forward solution, a field of voltages at all points of a 2mm-spaced grid of the head.

% 19967 dipoles for left hemisphere

mod = [212,0,88;213,0,89;214,0,90;215,0,85;216,0,86;217,0,87;152,0,28;153,0,29;154,0,30;155,0,25;156,0,26;157,0,27;236,0,112;237,0,113;238,0,114;239,0,109;240,0,110;241,0,111;230,0,106;231,0,107;232,0,108;233,0,103;234,0,104;235,0,105;170,0,51;171,0,53;172,0,54;173,0,43;174,0,44;175,0,45;176,0,52;177,0,49;178,0,50;179,0,46;180,0,47;181,0,48;224,0,100;225,0,101;226,0,102;227,0,97;228,0,98;229,0,99;218,0,94;219,0,95;220,0,96;221,0,91;222,0,92;223,0,93;182,0,58;183,0,59;184,0,60;185,0,55;186,0,56;187,0,57;194,0,70;195,0,71;196,0,72;197,0,67;198,0,68;199,0,69;188,0,64;189,0,65;190,0,66;191,0,61;192,0,62;193,0,63;200,0,76;201,0,77;202,0,78;203,0,73;204,0,74;205,0,75;206,0,82;207,0,83;208,0,84;209,0,79;210,0,80;211,0,81];
% tracking matrix for scirun module numbers
numElec=19967;

for i=1:numElec
    fid = fopen(['/Users/dessertgrace/Documents/project_seeg_discernable_signal_grill_-selected/DipoleAutoScripts/DipoleScript' num2str(i) '.py'],'w');

    fprintf(fid,'import os\n');
    fprintf(fid,['scirun_load_network("/hpc/group/wmglab/ged12/cluster/DiffRec-PatientForwardDipoleAllAutomation.srn5")\n']);
    fprintf(fid,['scirun_set_module_state("GetMatrixSlice:0","SliceIndex",' num2str(i-1) ')\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:94","Filename", "/hpc/group/wmglab/ged12/cluster/HeadTetMesh.mat")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:3","Filename","/hpc/group/wmglab/ged12/cluster/ConductivityTensors.mat")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:4","Filename","/hpc/group/wmglab/ged12/cluster/SkinFinal.stl")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:128","Filename","/hpc/group/wmglab/ged12/cluster/lh_normals_20k.mat")\n']);
    
    fprintf(fid,['scirun_set_module_state("ReadField:93","Filename","/hpc/group/wmglab/ged12/cluster/run_grid_soln_PatientForwardDipoleAll.mat")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:0","FileTypeName", "Matlab Matrix")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:0","Filename","/work/ged12/DipoleData/T0/HeadDipoleVoltageSampledAtRecLoc' num2str(i) '.mat")\n']);

    for k=1:78
        fprintf(fid,['scirun_set_module_state("ReadField:' num2str(mod(k,1)) '","FileTypeName", "Matlab Field")\n']);
        fprintf(fid,['scirun_set_module_state("ReadField:' num2str(mod(k,1)) '","Filename","/work/ged12/DiffTGrids/diff_trans_grid_' num2str(k) '.mat")\n']);
       
        fprintf(fid,['scirun_set_module_state("WriteMatrix:' num2str(mod(k,3)) '","FileTypeName", "Matlab Matrix")\n']);
        fprintf(fid,['scirun_set_module_state("WriteMatrix:' num2str(mod(k,3)) '","Filename","/work/ged12/DipoleData/T' num2str(k) '/Trans_DiffRec' num2str(i) '_' num2str(k) '.mat")\n']);
    end
    
    fprintf(fid,['scirun_save_network("/work/ged12/DipoleNetworks/DiffRec-PatientForwardDipoleAllAutomation' num2str(i) '.srn5")\n']);
    fclose(fid);
end

