%% create Python scripts to make SCIRun Networks to run dipoles

% ************************************************************************
% 3/16/21
% Grace Dessert
% ************************************************************************

% create 9600 python scripts to create 9600 SCIRun networks to run 3200 centroid unit dipoles 
%   for each resolution of cortex surfaces (20k, 145k, full resolution)
% create all python scripts for all elementary/face dipoles in cortex surface to get all single 
%   dipole forward solutions needed to create patch forward solutions. 

% run on the Duke Computing Cluster


%% write python scripts!

% 3 resolutions
% 20 LH patch locs, 20 sizes of patches at each
% save these in whole cortex face basis (not index of dipole basis)
% in case I want to use them later for something else

% 400 LH centroids -> 1600 single dipole current sources (4 dirs each)
% centroid.node/field
% times 2 for RH
% times 3 for three resolutions = 9600 total

% 145k surfaces

% patch model / normal dipole model scripts
local_or_cluster = 2;
if local_or_cluster ==1
    load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/dipoleIndsLH_all.mat','dipoleIndsLH')
    load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/dipoleIndsRH_all.mat','dipoleIndsRH')
else
    load('/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/dipoleIndsLH_all.mat','dipoleIndsLH')
    load('/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/dipoleIndsRH_all.mat','dipoleIndsRH')
end
dipoleIndsLH = dipoleIndsLH{2};
dipoleIndsRH = dipoleIndsRH{2};

% LH
for dipI = 1:length(dipoleIndsLH) % 28190
    dipole = dipoleIndsLH(dipI);
    name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/DipoleFaces/ContactVoltage/HeadDipoleVoltage_ContactLocs_' num2str(dipole) '.mat']);
    % if this dipole has already been run (data exists), don't create a script
    % therefore we will later be attempting to run scripts and networks that don't exist, but that is ok.  
    if ~isfile(name) % file does not exist
    % save scripts and networks with dipI indexing 1 to length of dipoles im running here
    % save data with dipole indexing. 1 to total num faces in LH surface
    fid = fopen(['/work/ged12/CentroidModel/HigherRes/Scripts/DipoleFaces/CentroidModelScript' num2str(dipI) '.py'],'w');
    fprintf(fid,'import os\n');
    fprintf(fid,['scirun_load_network("/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/PatientForwardDipoleAllAutomation_centroidmodel_HigherRes_ContactLocs.srn5")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:3","Filename", "/hpc/group/wmglab/ged12/cluster/ConductivityTensors.mat")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:133","Filename", "/hpc/group/wmglab/ged12/cluster/ContactLocations.mat")\n']);
    fprintf(fid,['scirun_set_module_state("GetMatrixSlice:0","SliceIndex",' num2str(dipI-1) ')\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:94","Filename", "/hpc/group/wmglab/ged12/cluster/HeadTetMesh.mat")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:4","Filename","/hpc/group/wmglab/ged12/cluster/SkinFinal.stl")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:128","Filename","/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/dipoles_LH_centroidModel_highRes.mat")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:93","Filename","/hpc/group/wmglab/ged12/cluster/run_grid_soln_PatientForwardDipoleAll.mat")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:0","FileTypeName", "Matlab Matrix")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:0","Filename","/work/ged12/CentroidModel/HigherRes/Data/DipoleFaces/FS/HeadDipoleVoltage_' num2str(dipole) '.mat")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:1","FileTypeName", "Matlab Matrix")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:1","Filename","/work/ged12/CentroidModel/HigherRes/Data/DipoleFaces/ContactVoltage/HeadDipoleVoltage_ContactLocs_' num2str(dipole) '.mat")\n']);
    fprintf(fid,['scirun_save_network("/work/ged12/CentroidModel/HigherRes/Networks/DipoleFaces/PatientForwardDipoleAllAutomation_centroidmodel_HigherRes_ContactLocs' num2str(dipI) '.srn5")\n']);
    fclose(fid);
    end
end

% RH
for dipI = 1:length(dipoleIndsRH) % 27163
    dipole = dipoleIndsRH(dipI);
    name = sprintf(['/work/ged12/CentroidModel/HigherRes/Data/DipoleFaces/ContactVoltage/HeadDipoleVoltageR_ContactLocs_' num2str(dipole) '.mat']);
    % if this dipole has already been run (data exists), don't create a script
    % therefore we will later be attempting to run scripts and networks that don't exist, but that is ok.  
    if ~isfile(name) % file does not exist
    % save scripts and networks with dipI indexing 1 to length of dipoles im running here
    % save data with dipole indexing. 1 to total num faces in LH surface
    fid = fopen(['/work/ged12/CentroidModel/HigherRes/Scripts/DipoleFaces/CentroidModelScriptR' num2str(dipI) '.py'],'w');
    fprintf(fid,'import os\n');
    fprintf(fid,['scirun_load_network("/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/PatientForwardDipoleAllAutomationR_centroidmodel_HigherRes_ContactLocs.srn5")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:3","Filename", "/hpc/group/wmglab/ged12/cluster/ConductivityTensors.mat")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:133","Filename", "/hpc/group/wmglab/ged12/cluster/ContactLocations.mat")\n']);
    fprintf(fid,['scirun_set_module_state("GetMatrixSlice:0","SliceIndex",' num2str(dipI-1) ')\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:94","Filename", "/hpc/group/wmglab/ged12/cluster/HeadTetMesh.mat")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:4","Filename","/hpc/group/wmglab/ged12/cluster/SkinFinal.stl")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:128","Filename","/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/dipoles_RH_centroidModel_highRes.mat")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:93","Filename","/hpc/group/wmglab/ged12/cluster/run_grid_soln_PatientForwardDipoleAll.mat")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:0","FileTypeName", "Matlab Matrix")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:0","Filename","/work/ged12/CentroidModel/HigherRes/Data/DipoleFaces/FS/HeadDipoleVoltageR_' num2str(dipole) '.mat")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:1","FileTypeName", "Matlab Matrix")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:1","Filename","/work/ged12/CentroidModel/HigherRes/Data/DipoleFaces/ContactVoltage/HeadDipoleVoltageR_ContactLocs_' num2str(dipole) '.mat")\n']);
    fprintf(fid,['scirun_save_network("/work/ged12/CentroidModel/HigherRes/Networks/DipoleFaces/PatientForwardDipoleAllAutomationR_centroidmodel_HigherRes_ContactLocs' num2str(dipI) '.srn5")\n']);
    fclose(fid);
    end
end


%% full resolution surfaces

% patch model / normal dipole model scripts
local_or_cluster = 2;
if local_or_cluster ==1
    load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/dipoleIndsLH_all.mat','dipoleIndsLH')
    load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/dipoleIndsRH_all.mat','dipoleIndsRH')
else
    load('/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/dipoleIndsLH_all.mat','dipoleIndsLH')
    load('/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/dipoleIndsRH_all.mat','dipoleIndsRH')
end
dipoleIndsLH = dipoleIndsLH{1};
dipoleIndsRH = dipoleIndsRH{1};

count = 0;
for p = 1:length(dipoleIndsLH)
    dipole = dipoleIndsLH(p);
    name = sprintf(['/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/Data/ContactLocs/HeadDipoleVoltage_ContactVoltage_fullResDipole' num2str(dipole) '.mat']);
    % if this dipole has already been run (data exists), don't create a script
    % therefore we will later be attempting to run scripts and networks that don't exist, but that is ok.  
    if ~isfile(name) % file does not exist
        % save scripts and networks with dipI indexing 1 to length of dipoles im running here
        % save data with dipole indexing. 1 to total num faces in LH surface
        if p > 50000
            % if we have more jobs than maxArraySize, change naming
            % structure so it can be submitted on slurm
            pp = p-50000;
            fid = fopen(['/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/Scripts/PatchResModelScript_2_' num2str(pp) '.py'],'w');
            fprintf(fid,'import os\n');
            fprintf(fid,['scirun_load_network("/hpc/group/wmglab/ged12/cluster/CentroidModel/PatchResolutionTesting/PatientForwardDipoleAllAutomation_PatchRes.srn5")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:3","Filename", "/hpc/group/wmglab/ged12/cluster/ConductivityTensors.mat")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:133","Filename", "/hpc/group/wmglab/ged12/cluster/ContactLocations.mat")\n']);
            fprintf(fid,['scirun_set_module_state("GetMatrixSlice:0","SliceIndex",' num2str(p-1) ')\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:94","Filename", "/hpc/group/wmglab/ged12/cluster/HeadTetMesh.mat")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:4","Filename","/hpc/group/wmglab/ged12/cluster/SkinFinal.stl")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:128","FileTypeName", "Matlab Field")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:128","Filename","/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/dipoles_LH_centroidModel_fullRes.mat")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:93","Filename","/hpc/group/wmglab/ged12/cluster/run_grid_soln_PatientForwardDipoleAll.mat")\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:0","FileTypeName", "Matlab Matrix")\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:0","Filename","/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/Data/FS/HeadDipoleVoltage_fullResDipole' num2str(dipole) '.mat")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","MaxIterations","10000")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","ProgrammableInputPortEnabled","0")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","Method","cg")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","Preconditioner","Jacobi")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","TargetError","1e-05")\n']); 
            fprintf(fid,['scirun_set_module_state("WriteMatrix:1","FileTypeName", "Matlab Matrix")\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:1","Filename","/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/Data/ContactLocs/HeadDipoleVoltage_ContactVoltage_fullResDipole' num2str(dipole) '.mat")\n']);
            fprintf(fid,['scirun_save_network("/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/Networks/PatientForwardDipoleAllAutomation_PatchRes_2_' num2str(pp) '.srn5")\n']);
            fclose(fid);
        else
            fid = fopen(['/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/Scripts/PatchResModelScript' num2str(p) '.py'],'w');
            fprintf(fid,'import os\n');
            fprintf(fid,['scirun_load_network("/hpc/group/wmglab/ged12/cluster/CentroidModel/PatchResolutionTesting/PatientForwardDipoleAllAutomation_PatchRes.srn5")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:3","Filename", "/hpc/group/wmglab/ged12/cluster/ConductivityTensors.mat")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:133","Filename", "/hpc/group/wmglab/ged12/cluster/ContactLocations.mat")\n']);
            fprintf(fid,['scirun_set_module_state("GetMatrixSlice:0","SliceIndex",' num2str(p-1) ')\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:94","Filename", "/hpc/group/wmglab/ged12/cluster/HeadTetMesh.mat")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:4","Filename","/hpc/group/wmglab/ged12/cluster/SkinFinal.stl")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:128","FileTypeName", "Matlab Field")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:128","Filename","/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/dipoles_LH_centroidModel_fullRes.mat")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:93","Filename","/hpc/group/wmglab/ged12/cluster/run_grid_soln_PatientForwardDipoleAll.mat")\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:0","FileTypeName", "Matlab Matrix")\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:0","Filename","/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/Data/FS/HeadDipoleVoltage_fullResDipole' num2str(dipole) '.mat")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","MaxIterations","10000")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","ProgrammableInputPortEnabled","0")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","Method","cg")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","Preconditioner","Jacobi")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","TargetError","1e-05")\n']); 
            fprintf(fid,['scirun_set_module_state("WriteMatrix:1","FileTypeName", "Matlab Matrix")\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:1","Filename","/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/Data/ContactLocs/HeadDipoleVoltage_ContactVoltage_fullResDipole' num2str(dipole) '.mat")\n']);
            fprintf(fid,['scirun_save_network("/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/Networks/PatientForwardDipoleAllAutomation_PatchRes' num2str(p) '.srn5")\n']);
            fclose(fid);
        end
    else
        % this data already exists
        % clear the python script in case it already exists for another
        % dipole
        fid = fopen(['/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/Scripts/PatchResModelScript' num2str(p) '.py'],'w');
        fclose(fid);
        count = count + 1;
    end
end
disp([num2str(count) ' dipoles of ' num2str(length(dipoleIndsLH)) ' dipoles have already been run'])

% RH 
count = 0;
for p = 1:length(dipoleIndsRH)
    dipole = dipoleIndsRH(p);
    name = sprintf(['/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/DataR/ContactLocs/HeadDipoleVoltage_ContactVoltage_fullResDipoleR' num2str(dipole) '.mat']);
    % if this dipole has already been run (data exists), don't create a script
    % therefore we will later be attempting to run scripts and networks that don't exist, but that is ok.  
    if ~isfile(name) % file does not exist
        % save scripts and networks with dipI indexing 1 to length of dipoles im running here
        % save data with dipole indexing. 1 to total num faces in LH surface
        if p > 50000
            % if we have more jobs than maxArraySize, change naming
            % structure so it can be submitted on slurm
            pp = p-50000;
            fid = fopen(['/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/ScriptsR/PatchResModelScriptR_2_' num2str(pp) '.py'],'w');
            fprintf(fid,'import os\n');
            fprintf(fid,['scirun_load_network("/hpc/group/wmglab/ged12/cluster/CentroidModel/PatchResolutionTesting/PatientForwardDipoleAllAutomation_PatchRes.srn5")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:3","Filename", "/hpc/group/wmglab/ged12/cluster/ConductivityTensors.mat")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:133","Filename", "/hpc/group/wmglab/ged12/cluster/ContactLocations.mat")\n']);
            fprintf(fid,['scirun_set_module_state("GetMatrixSlice:0","SliceIndex",' num2str(p-1) ')\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:94","Filename", "/hpc/group/wmglab/ged12/cluster/HeadTetMesh.mat")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:4","Filename","/hpc/group/wmglab/ged12/cluster/SkinFinal.stl")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:128","FileTypeName", "Matlab Field")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:128","Filename","/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/dipoles_RH_centroidModel_fullRes.mat")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:93","Filename","/hpc/group/wmglab/ged12/cluster/run_grid_soln_PatientForwardDipoleAll.mat")\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:0","FileTypeName", "Matlab Matrix")\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:0","Filename","/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/DataR/FS/HeadDipoleVoltage_fullResDipoleR' num2str(dipole) '.mat")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","MaxIterations","10000")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","ProgrammableInputPortEnabled","0")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","Method","cg")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","Preconditioner","Jacobi")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","TargetError","1e-05")\n']); 
            fprintf(fid,['scirun_set_module_state("WriteMatrix:1","FileTypeName", "Matlab Matrix")\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:1","FileTypeName", "Matlab Matrix")\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:1","Filename","/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/DataR/ContactLocs/HeadDipoleVoltage_ContactVoltage_fullResDipoleR' num2str(dipole) '.mat")\n']);
            fprintf(fid,['scirun_save_network("/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/NetworksR/PatientForwardDipoleAllAutomation_PatchResR_2_' num2str(pp) '.srn5")\n']);
            fclose(fid);
        else
            fid = fopen(['/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/ScriptsR/PatchResModelScriptR' num2str(p) '.py'],'w');
            fprintf(fid,'import os\n');
            fprintf(fid,['scirun_load_network("/hpc/group/wmglab/ged12/cluster/CentroidModel/PatchResolutionTesting/PatientForwardDipoleAllAutomation_PatchRes.srn5")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:3","Filename", "/hpc/group/wmglab/ged12/cluster/ConductivityTensors.mat")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:133","Filename", "/hpc/group/wmglab/ged12/cluster/ContactLocations.mat")\n']);
            fprintf(fid,['scirun_set_module_state("GetMatrixSlice:0","SliceIndex",' num2str(p-1) ')\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:94","Filename", "/hpc/group/wmglab/ged12/cluster/HeadTetMesh.mat")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:4","Filename","/hpc/group/wmglab/ged12/cluster/SkinFinal.stl")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:128","FileTypeName", "Matlab Field")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:128","Filename","/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/dipoles_RH_centroidModel_fullRes.mat")\n']);
            fprintf(fid,['scirun_set_module_state("ReadField:93","Filename","/hpc/group/wmglab/ged12/cluster/run_grid_soln_PatientForwardDipoleAll.mat")\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:0","FileTypeName", "Matlab Matrix")\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:0","Filename","/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/DataR/FS/HeadDipoleVoltage_fullResDipoleR' num2str(dipole) '.mat")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","MaxIterations","10000")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","ProgrammableInputPortEnabled","0")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","Method","cg")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","Preconditioner","Jacobi")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","TargetError","1e-05")\n']); 
            fprintf(fid,['scirun_set_module_state("WriteMatrix:1","FileTypeName", "Matlab Matrix")\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:1","FileTypeName", "Matlab Matrix")\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:1","Filename","/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/DataR/ContactLocs/HeadDipoleVoltage_ContactVoltage_fullResDipoleR' num2str(dipole) '.mat")\n']);
            fprintf(fid,['scirun_save_network("/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/NetworksR/PatientForwardDipoleAllAutomation_PatchResR' num2str(p) '.srn5")\n']);
            fclose(fid);
        end
    else
        % this data already exists
        % clear the python script in case it already exists for another
        % dipole
        fid = fopen(['/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/ScriptsR/PatchResModelScriptR' num2str(p) '.py'],'w');
        fclose(fid);
        count = count + 1;
    end
end
disp([num2str(count) ' dipoles of ' num2str(length(dipoleIndsRH)) ' dipoles have already been run'])


%%
% centroid model scripts
% LH

for p = 1:4800
    fid = fopen(['/work/ged12/CentroidModel/HigherRes/Scripts/Centroids/CentroidModelScript' num2str(p) '.py'],'w');
    fprintf(fid,'import os\n');
    fprintf(fid,['scirun_load_network("/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/PatientForwardDipoleAllAutomation_centroidmodel_HigherRes_ContactLocs.srn5")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:3","Filename", "/hpc/group/wmglab/ged12/cluster/ConductivityTensors.mat")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:133","Filename", "/hpc/group/wmglab/ged12/cluster/ContactLocations.mat")\n']);
    %fprintf(fid,['scirun_set_module_state("GetMatrixSlice:0","SliceIndex",' num2str(p-1) ')\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:94","Filename", "/hpc/group/wmglab/ged12/cluster/HeadTetMesh.mat")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:4","Filename","/hpc/group/wmglab/ged12/cluster/SkinFinal.stl")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:128","Filename","/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/CentroidDipoleSingleFields_LH/CentroidField_LH' num2str(p) '.mat")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:93","Filename","/hpc/group/wmglab/ged12/cluster/run_grid_soln_PatientForwardDipoleAll.mat")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:0","FileTypeName", "Matlab Matrix")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:0","Filename","/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltage_' num2str(p) '.mat")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:1","FileTypeName", "Matlab Matrix")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:1","Filename","/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltage_ContactLocs_' num2str(p) '.mat")\n']);
    fprintf(fid,['scirun_save_network("/work/ged12/CentroidModel/HigherRes/Networks/Centroids/PatientForwardDipoleAllAutomation_centroidmodel_HigherRes_ContactLocs' num2str(p) '.srn5")\n']);
    fclose(fid);
end
% RH
for p = 1:4800
    fid = fopen(['/work/ged12/CentroidModel/HigherRes/Scripts/Centroids/CentroidModelScriptR' num2str(p) '.py'],'w');
    fprintf(fid,'import os\n');
    fprintf(fid,['scirun_load_network("/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/PatientForwardDipoleAllAutomationR_centroidmodel_HigherRes_ContactLocs.srn5")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:3","Filename", "/hpc/group/wmglab/ged12/cluster/ConductivityTensors.mat")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:133","Filename", "/hpc/group/wmglab/ged12/cluster/ContactLocations.mat")\n']);
    %fprintf(fid,['scirun_set_module_state("GetMatrixSlice:0","SliceIndex",' num2str(p-1) ')\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:94","Filename", "/hpc/group/wmglab/ged12/cluster/HeadTetMesh.mat")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:4","Filename","/hpc/group/wmglab/ged12/cluster/SkinFinal.stl")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:128","Filename","/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/CentroidDipoleSingleFields_RH/CentroidField_RH' num2str(p) '.mat")\n']);
    fprintf(fid,['scirun_set_module_state("ReadField:93","Filename","/hpc/group/wmglab/ged12/cluster/run_grid_soln_PatientForwardDipoleAll.mat")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:0","FileTypeName", "Matlab Matrix")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:0","Filename","/work/ged12/CentroidModel/HigherRes/Data/Centroids/FS/HeadDipoleVoltageR_' num2str(p) '.mat")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:1","FileTypeName", "Matlab Matrix")\n']);
    fprintf(fid,['scirun_set_module_state("WriteMatrix:1","Filename","/work/ged12/CentroidModel/HigherRes/Data/Centroids/ContactVoltage/HeadDipoleVoltageR_ContactLocs_' num2str(p) '.mat")\n']);
    fprintf(fid,['scirun_save_network("/work/ged12/CentroidModel/HigherRes/Networks/Centroids/PatientForwardDipoleAllAutomationR_centroidmodel_HigherRes_ContactLocs' num2str(p) '.srn5")\n']);
    fclose(fid);
end





