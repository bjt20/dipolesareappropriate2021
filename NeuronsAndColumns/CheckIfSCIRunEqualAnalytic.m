load D:\CorticalNeuronDipole\AllNeurons\CurrentInj\maxH_currinjection.mat
current=(data_all{19}.i_mem_matrix.*data_all{19}.areas');
current=current.*1e-2;%nA
load(['D:\CorticalNeuronDipole\AllNeurons\CurrentInjSphereVoltages\Myel\Neuron_regBox' num2str(19) '_SphereVoltages_Iso.mat']);
data=scirunfield.field;

% load('SpherePointsMultiTrial.mat');% Load in locations corresponding to data
locsOrig=scirunfield.node;

for i=1:length(locsOrig)
    coords=data_all{19}.coordinates./1000;
    rinv=1./sqrt(sum((coords-locsOrig(:,i)').^2,2));
    vm(i)=sum(current(121,:)'./(4.*pi).*rinv);
end

