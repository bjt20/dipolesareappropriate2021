Localizations=[];
load ForwardRepeatedTrials\RecordingLocations.mat
loc=scirunfield.node;
names={'IsotropicNeuron','IsotropicNeuron_Time2',...
    'IsotropicColumn','IsotropicDipole',...
    'AnsotropicNeuron','AnsotropicNeuron_Time2',...
    'AnisotropicColumn','AnisotropicDipole',...
    'IsotropicNeuron_withAxon_81','IsotropicNeuron_withAxon_106',...
    'IsotropicNeuron_withAxon_128','AnisotropicNeuron_withAxon_81',...
    'AnisotropicNeuron_withAxon_106','AnisotropicNeuron_withAxon_128'};
noise={'','_20db','_5db'};
dataFin=[];
for k=1:3
    for j=1:14
        for i=1:50
            name=['SourceLocalizationsRepeatedTrials\' names{j} '_' num2str(i) noise{k} '.mat'];
            load(name)
            %         figure
            %
            %         hold on
            %         plot3(scirunfield.node(1,scirunmatrix>0.99.*max(scirunmatrix)),...
            %             scirunfield.node(2,scirunmatrix>0.99.*max(scirunmatrix))...
            %             ,scirunfield.node(3,scirunmatrix>0.99.*max(scirunmatrix)),'g*')
            [val,ind]=max(scirunmatrix);
            %         plot3(0,0,0,'k*')
            %         plot3(scirunfield.node(1,ind),scirunfield.node(2,ind),scirunfield.node(3,ind),'r*')
            %         view(-16,28)
            Localizations(j,i)=sqrt(sum(scirunfield.node(:,ind).^2));
        end
    end
    dataFin=[dataFin;Localizations];
    figure
    hold on
    data=[];
    outliersX=[];
    outliersY=[];
    ct=1;
    StdError=[];
    for i=[1:4 9:11]
        %     data(ct)=mean(Localizations(i,~isoutlier(Localizations(i,:))));
        data(ct)=mean(Localizations(i,:));
        StdError(ct)=std(Localizations(i,:))./sqrt(50);
        %     bar(i,Localizations(median()))
        outliersX=[outliersX ct+0.*Localizations(i,isoutlier(Localizations(i,Localizations(4,:)<0.1)))];
        outliersY=[outliersY Localizations(i,isoutlier(Localizations(i,Localizations(4,:)<0.1)))];
        ct=ct+1;
        %     histogram(Localizations(i,:),0:0.1:16)
    end
    data=data([4 1 2 3 5 6 7]);
    bar(data)
    hold on
    e=errorbar(1:7,data,StdError);
    e.Color=[0 0 0];
    e.LineStyle='none';
    ylabel('Localization Error (mm)')
    xticks(1:7)
    xtickangle(30)
    name={};
    t=1;
    for i=[4 1:3 9:11]
        name{t}=names{i};
        t=t+1;
    end
    xticklabels(name)
    axis([0 8 0 inf])
    fileName=['Figures\IsotropicSourceLocalization' noise{k} '.pdf'];
    %     print(gcf,fileName,'-bestfit','-dpdf');
    % scatter(outliersX,outliersY)
    
    figure
    hold on
    data=[];
    outliersX=[];
    outliersY=[];
    ct=1;
    StdError=[];
    for i=[5:8 12:14]
        data(ct)=mean(Localizations(i,:));
        StdError(ct)=std(Localizations(i,:))./sqrt(50);
        %     bar(i,Localizations(median()))
        %     outliersX=[outliersX ct+0.*Localizations(i,isoutlier(Localizations(i,Localizations(4,:)<0.1)))];
        %     outliersY=[outliersY Localizations(i,isoutlier(Localizations(i,Localizations(4,:)<0.1)))];
        ct=ct+1;
    end
    data=data([4 1 2 3 5 6 7]);
    bar(data)
    hold on
    e=errorbar(1:7,data,StdError);
    e.Color=[0 0 0];
    e.LineStyle='none';
    ylabel('Localization Error (mm)')
    xticks(1:7)
    t=1;
    name={};
    for i=[8 5:7 12:14]
        name{t}=names{i};
        t=t+1;
    end
    xticklabels(name)
    xtickangle(30)
    axis([0 8 0 inf])
    fileName=['Figures\AnisotropicSourceLocalization' noise{k} '.pdf'];
    %     print(gcf,fileName,'-bestfit','-dpdf');
    % scatter(outliersX,outliersY)
    
    % boxplot(Localizations')
    
    % plot(median(Localizations'),'k*')
end
%%
% Localizations=[];
axontype={'NoAxon','Unmyel','Myel'};
medium={'Isotropic','Anisotropic'};
noise={'','_20db','_5db'};
load ForwardRepeatedTrials\RecordingLocations.mat
loc=scirunfield.node;
dataFin=zeros(3,2,3,5,5,10);
noiseLevel=zeros(3,2,3,5,5,10);
mediumType=zeros(3,2,3,5,5,10);
Axon=zeros(3,2,3,5,5,10);
CellType=zeros(3,2,3,5,5,10);
sensorConfig=zeros(3,2,3,5,5,10);
for q = 1%:3 %noise levels
    for k=1:2 % iso aniso
        for p=1:3 % Axon type
            for j=1:5 % cell type
                for y=1:5 % cell Num
                    for i=1:10 % sensor config
                        if p==2 & j==3 & y==1
                            name=['AllNeurons\CurrentInjLocalizations\' axontype{p} '\Neuron_' medium{k} num2str((j-1)*5+y) '_' num2str(i) '_Regularized_LowerMesh.mat'];
                        else
                            name=['AllNeurons\CurrentInjLocalizations\' axontype{p} '\Neuron_' medium{k} num2str((j-1)*5+y) '_' num2str(i) '_Regularized' noise{q} '.mat'];
                        end
                        load(name)
                        [val,ind]=max(scirunmatrix);
                        Localizations=sqrt(sum(scirunfield.node(:,ind).^2));
                        dataFin(q,k,p,j,y,i)=Localizations;
                        noiseLevel(q,k,p,j,y,i)=q;
                        mediumType(q,k,p,j,y,i)=k;
                        Axon(q,k,p,j,y,i)=p;
                        CellType(q,k,p,j,y,i)=j;
                        sensorConfig(q,k,p,j,y,i)=i;
                    end
                    dataFin(q,k,p,j,y,i)=mean(Localizations);
                end
            end
            %             dataFin=[dataFin;Localizations];
        end
    end
end
%%
a=[];
b=[];
c=[];
d=[];
e=[];
f=[];
dataFine=[];
for q = 1%:3 %noise levels
    for k=1:2 % iso aniso
        for p=1:3 % Axon type
            for j=1:5 % cell type
                for y=1:5 % cell Num
                    a(q,k,p,j,y)=mean(dataFin(q,k,p,j,y,1:10));
                    b(q,k,p,j,y)=mean(noiseLevel(q,k,p,j,y,1:10));
                    c(q,k,p,j,y)=mean(mediumType(q,k,p,j,y,1:10));
                    d(q,k,p,j,y)=mean(Axon(q,k,p,j,y,1:10));
                    e(q,k,p,j,y)=mean(CellType(q,k,p,j,y,1:10));
                    f(q,k,p,j,y)=mean(sensorConfig(q,k,p,j,y,1:10));
                end
            end
            dataFine=[dataFine;Localizations];
        end
    end
end
%%
for i=11:20
    name=['SourceLocalizationsRepeatedTrials\AnisotropicDipole_' num2str(i) '.mat'];
    load(name)
    [val,ind]=max(scirunmatrix);
    Localizations=sqrt(sum(scirunfield.node(:,ind).^2))
end
%%
ct=1;
for i=1:2
            figure
%     for j=1:3
        data=squeeze(a(1,i,1,:,:))';
        %         bar(mean(reshape(mean(data),5,5)))
        %         hold on
        hold on
        plot(1,6.1535e-15,'ro')
        if i==1
            plot(2,0.4040,'bo')
        else
            plot(2,0.5714,'bo')
        end
        for q=1:5
            hold on
            plot(2 + q+[-0.2 -0.1 0 0.1 0.2],data(:,q),'ko');
        end
        data=squeeze(a(1,i,2,:,:))';
        for q=1:5
            hold on
            plot(2 + q+1/30+[-0.2 -0.1 0 0.1 0.2],data(:,q),'mo');
        end
        data=squeeze(a(1,i,3,:,:))';
        for q=1:5
            hold on
            plot(2+q+1/15+[-0.2 -0.1 0 0.1 0.2],data(:,q),'co');
        end
%         title([medium{i} ' ' axontype{j}])
        ct=ct+1;
        xticks([1 2 3 4 5 6 7])
        xticklabels({'Dipole','Column','L1','L2/3','L4','L5','L6'})
        axis([0 8 0 12])
        fileName=['Paper\Figures\' medium{i} 'SourceLocalization_FullRegularization_NoNoise.pdf'];
        print(gcf,fileName,'-bestfit','-dpdf');
%     end
end