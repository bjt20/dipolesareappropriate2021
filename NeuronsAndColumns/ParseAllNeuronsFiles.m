%% Load data into data_all
% load AllNeurons\SynapticActivation\umaxH_synapact.mat
% load AllNeurons\SynapticActivation\maxH_rax1_synapact.mat
load AllNeurons\SynapticActivation\maxH_synapact.mat
% load AllNeurons\CurrentInj\umaxH_currinjection.mat
% load AllNeurons\CurrentInj\maxH_rax1_currinjection.mat
% load AllNeurons\CurrentInj\maxH_currinjection.mat
scirunfield=struct;
% figure
% hold on
%%
% index=21;
% C=data_all{index}.coordinates;
% vm=data_all{index}.vm_matrix;
% for t=1:400
    %     scatter3(C(:,1),C(:,2),C(:,3),ones(1,length(i_mems(1,:))).*10.*abs(i_mems(t,:)),i_mems(t,:)>0,'Filled')%.*1000.*abs(i_mems(t,:))
%     scatter3(C(:,1),C(:,2),C(:,3),vm(t,:)-min(min(vm))+0.01,vm(t,:),'Filled')
%     %     colorbar
%     caxis([-70 -50])
%     colormap jet
%     title(num2str(t*0.025))
%     %     xlabel('unmyel')
%     view(101,26)
%     axis([-200 200 -500 500 -400 400])
%     axis equal
%     pause(0.00001)
%     %     animation_frames{end+1} = getframe(gcf);
% end
%%

for i=1:25
    figure
     scirunfield.node=data_all{i}.coordinates'./1000;
    rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 0 10]).^2,2));
%     rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 10 0]).^2,2)); % No Axon
    current=(data_all{i}.i_mem_matrix.*data_all{i}.areas');
    current(:,1)=current(:,1)-data_all{i}.i_mem_matrix*data_all{i}.areas;
    current=current.*1e-2;%nA
    voltageTrace=current*rinv;
%     figure(i)
    hold on
    plot(voltageTrace)
    title(num2str(i))
    legend('Curr Inj No Axon','Curr Inj Unmyel','Synap Act No Axon','Synap Act Unmyel')
end
% %%
% scirunfield.node=data_all{i}.coordinates'./1000;
% rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 0 10]).^2,2));
% %     rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 10 0]).^2,2)); % No Axon
% current=(data_all{i}.i_mem_matrix.*data_all{i}.areas');
% current(:,1)=current(:,1)-data_all{i}.i_mem_matrix*data_all{i}.areas;
% current=current.*1e-2;%nA
% moment=[sum(current'.*scirunfield.node(1,:)');sum(current'.*scirunfield.node(2,:)');sum(current'.*scirunfield.node(3,:)')];
% figure
% ampMoment=sqrt(sum(moment.^2));
% plot(ampMoment)
% hold on
% ampMoment=sum(moment);
% plot(ampMoment)
%%
savet=[];
savet2=[];
EPSCTimes=[3083 405 2415 2944 1541 749 613 474 575 405 2787 2756 3533 2439 1951 178 217 193 386 244 1976 216 1195 352 282];
for i=1:25%1:length(data_all)
%     figure
    scirunfield.node=data_all{i}.coordinates'./1000;
    rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 0 10]).^2,2)); 
%     rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 10 0]).^2,2)); % No Axon
    current=(data_all{i}.i_mem_matrix.*data_all{i}.areas');
    current(:,1)=current(:,1)-data_all{i}.i_mem_matrix*data_all{i}.areas;
    current=current.*1e-2;%nA
    voltageTrace=current*rinv;
    [m,t]=max(abs(voltageTrace));
    savet=[savet t];
    scirunfield.field=current(EPSCTimes(i),:);
    %     max(max(abs(current)))
%     plot(voltageTrace)
%     
%     rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 -10 0]).^2,2));
%     current=(data_all{i}.i_mem_matrix.*data_all{i}.areas');
%     current(:,1)=current(:,1)-data_all{i}.i_mem_matrix*data_all{i}.areas;
%     current=current.*1e-2;%nA
%     voltageTrace=current*rinv;
%     [m,t]=max(abs(voltageTrace));
%     savet2=[savet2 t];
%     hold on
%     plot(voltageTrace)
    %     pause
    %     sum(sum(current))
%         save(['AllNeurons\CurrentInjWithDataOnCoordSCI\Neuron' num2str(i) '_Unmyel_WithDataOnCoordMaxPoint.mat'],'scirunfield')
save(['AllNeurons\SynapActWithDataOnCoordSCI\Neuron' num2str(i) '_Myel_WithDataOnCoordEPSC.mat'],'scirunfield')
end