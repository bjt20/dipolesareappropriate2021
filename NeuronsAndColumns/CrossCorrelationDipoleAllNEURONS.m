% load('Spheres\SphereNEURON_81_withAxon.mat')
%% Initialize
radii=[0.1 0.5 1 2 3 4 5 6 7 8 9 10 15 20];
dipolyness=zeros(25,length(radii)-1);
posz=zeros(25,length(radii)-1);
% directions=[];
moment=zeros(25,length(radii)-1);
momentPosz=zeros(25,length(radii)-1);
directions=zeros(25,length(radii)-1);
%% Fits
% fo = fitoptions('Method','NonlinearLeastSquares',...
%     'Lower',[0],...
%     'Upper',[Inf],...
%     'StartPoint',[1]);
% ft = fittype('a*x','options',fo);
tic
AxonType={'NoAxon','Unmyel','Myel'};
ActivationType={'CurrentInj','SynapAct'};
for j=1%1:3
    for k=2%1:2
        for i=1:25
            load(['AllNeurons\' ActivationType{k} 'SphereVoltages\' AxonType{j} '\Neuron_regBox' num2str(i) '_SphereVoltages_Iso.mat']);
            dataOrig=scirunfield.field;% Load in Data for a given sphere
            load('SpherePointsMultiTrial.mat');% Load in locations corresponding to data
            locsOrig=scirunfield.node;
            radii=[0.1 0.5 1 2 3 4 5 6 7 8 9 10 15 20];
            %     dipolyness=zeros(1,length(radii)-1);
            %     posz=zeros(1,length(radii)-1);
%             directions=[];
            %     moment=zeros(1,length(radii)-1);
            %     for trial=1:50
            for index=1:length(radii)-1
                which=radii(index).*10==round(sqrt(sum(locsOrig.^2)).*10);
                data=dataOrig(which);
                locs=locsOrig(:,which);
                radius=radii(index);% radius of sphere
                Xcoor=zeros(360,360);
                Moments=zeros(360,360);
                %         data=data./max(data);
                for theta=1:360
                    for phi=1:180
                        direction=[cosd(theta).*cosd(phi) sind(theta).*cosd(phi) sind(phi)];
                        angle=sum(direction.*locs',2)./(sqrt(sum(locs.^2))'.*sqrt(sum(direction.^2)));
                        dipole=angle./(4.*pi.*radius'.^2);
                        Moments(theta,phi)=dipole\data';
                        %                 bootci(10,@scorr,dipole,data')
                        dipole=dipole'-mean(dipole);
                        Xcoor(theta,phi)=sum((data-mean(data)).*dipole)./(sqrt(sum(dipole.^2)).*sqrt(sum((data-mean(data)).^2)));
                        %                 [m,ind]=max(dipole);
                        %                 ms(index)=dipole'\data';
                        %                 errs(index)=dipole(ind).*(dipole'\data')-data(ind);
                        %                 RelErrs(index)=(dipole(ind).*(dipole'\data')-data(ind))./(dipole(ind).*(dipole'\data'));
                    end
                end
                if j==1
                    momentPosz(i,index)=Moments(90,180);
                    posz(i,index)=Xcoor(90,180);% x direction oriented (No axon)
                else
                    momentPosz(i,index)=Moments(90,90);
                    posz(i,index)=Xcoor(90,90);% z direction oriented (With axon)
                end
                %             posz(trial,index)=Xcoor(180,180);
                dipolyness(i,index)=max(max(abs(Xcoor)));
                dir=find(abs(Xcoor)==max(max(abs(Xcoor))));
                moment(i,index)=Moments(dir(1));
                [x,y]=ind2sub([360 360],dir(1));
                directions(i,index) = dir(1);
            end
            %     moment
            %     dipolyness
            % [x,y]=ind2sub([360 360],directions)
            % mean(x)
            % mean(y)
            % std(x)./sqrt(650)
            % std(y)./sqrt(650)
            % [cosd(mean(x)).*cosd(mean(y)) sind(mean(x)).*cosd(mean(y)) sind(mean(y))]
            % plot(radii(1:end-1),dipolyness.^2)
            %     figure
            %     plot(radii(1:end-1),dipolyness(i,:).^2)
            %     hold on
            %     plot(radii(1:end-1),posz(i,:).^2)
            %     axis([0 15 0 1])
            %     ylabel('Cross Correlation With Ideal Dipole')
            %     xlabel('Recording Radius')
            %     legend('Max','\Theta = 0 \Phi = 0')
            %     fileName=['Figures\XCoorNEURON_AllNeuron_AllTrialsCollected_BiggerBox_' num2str(i) '_Withpz_Unmyel.pdf'];
            % fileName='Figures\XCoorDipole_Withpz.pdf';
            % fileName='Figures\XCoorColumn_Withpz.pdf';
            %     print(gcf,fileName,'-bestfit','-dpdf');
            % surf(Xcoor)
            % max(max(Xcoor))
            
            %     figure
            %     plot(radii(1:end-1),abs(moment(i,:)))
            %     xlabel('Recording Radius')
            %     ylabel('Dipole Magnitude (pA)')
            %     fileName=['Figures\DipoleMagNEURON_AllNeuron_AllTrialsCollected_BestFit_BiggerBox_' num2str(i) '_Unmyel.pdf'];
            %     print(gcf,fileName,'-bestfit','-dpdf');
            disp(num2str(i))
        end
        save(['AllNeurons\' ActivationType{k} 'SphereVoltages\DipolynessAndMomentAll_AllDir_' AxonType{j} '.mat'],'dipolyness','posz','moment','momentPosz','directions');
        toc
    end
end
% toc

%%
% figure
% maxMom=data(:,1:12);
% plot(sum(maxMom,2),'k*')
figure
for i=1:5 % layers
    hold on
    errorbar(radii(1:end-1),mean(dipolyness(((i-1).*5+1):(i.*5),:).^2),std(dipolyness(((i-1).*5+1):(i.*5),:).^2)./sqrt(5))
    %     errorbar(radii(1:end-1),mean(dipolyness(((i-1).*5+1):(i.*5),:).^2),std(dipolyness(((i-1).*5+1):(i.*5),:).^2)./sqrt(5))
end
legend('I','II/III','IV','V','VI')
%
% figure
% for i=1:5 % layers
%     hold on
%     errorbar(radii(1:end-1),mean(posz(((i-1).*5+1):(i.*5),:).^2),std(posz(((i-1).*5+1):(i.*5),:).^2)./sqrt(5))
%     %     errorbar(radii(1:end-1),mean(dipolyness(((i-1).*5+1):(i.*5),:).^2),std(dipolyness(((i-1).*5+1):(i.*5),:).^2)./sqrt(5))
% end
% legend('I','II/III','IV','V','VI')
%
% figure
% for i=1:5 % layers
%     hold on
%     errorbar(radii(1:end-1),mean(abs(moment(((i-1).*5+1):(i.*5),:))),std(abs(moment(((i-1).*5+1):(i.*5),:)))./sqrt(5))
% end
% legend('I','II/III','IV','V','VI')
%
% figure
% for i=1:5 % layers
%     hold on
%     errorbar(radii(1:end-1),mean(abs(momentPosz(((i-1).*5+1):(i.*5),:))),std(abs(momentPosz(((i-1).*5+1):(i.*5),:)))./sqrt(5))
% end
% legend('I','II/III','IV','V','VI')

%%
% ct=1;
% name={};
% for i=[1 2 4 5 6]
%     for j=1:5
%         name{ct}=['L' num2str(i) '_' num2str(j)];
%         ct=ct+1;
%     end
% end