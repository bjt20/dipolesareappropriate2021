Folder = {'SynapticActivation','CurrentInj'};
AxonName = {'maxH_rax1','umaxH','maxH'};
activationName = {'synapact','currinjection'};
VTraces={};
ITraces={};
ITracesSoma={};
inds=[0 0 2 2 2 2 4 4 2 6 2 4 2 0 2 6 6 4 6 8 4 4 0 2 2];
load(['AllNeurons\' Folder{1} '\' AxonName{1} '_' activationName{1} '.mat'])
noAx=data_all;
for k = 1:2 % activation type
    for j = 1:3 % axon types
        load(['AllNeurons\' Folder{k} '\' AxonName{j} '_' activationName{k} '.mat'])
        for i=1:25 % Axon number
            if j == 1
                rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 10 0]).^2,2));
            else
                rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 0 10]).^2,2));
            end
            current=(data_all{i}.i_mem_matrix.*data_all{i}.areas');
%             current(:,1)=current(:,1)-data_all{i}.i_mem_matrix*data_all{i}.areas;
            current=current.*1e-2;%nA
            voltageTrace=current*rinv;
            VTraces{k,j,i}=voltageTrace;
            

            Somacurrent=[current(:,1).*0;current(:,1)];
            where=find(max(abs(Somacurrent))==abs(Somacurrent));
            Somacurrent = Somacurrent((where-200):(where+200));
            if j==1
                current=sum(current(:,end-1:end),2);
            elseif j==2
                current=sum(current(:,2:(end-length(noAx{i}.areas))),2);
            elseif j==3
                current=sum(current(:,inds(i)+length(noAx{i}.areas):end),2)+sum(current(:,2:(inds(i)+2)),2);
            end
            ITracesSoma{k,j,i}=Somacurrent;
            Axoncurrent=[current(:,1).*0;current(:,1)];
            where=find(max(abs(Axoncurrent))==abs(Axoncurrent));
            Axoncurrent = Axoncurrent((where-200):(where+200));
            ITraces{k,j,i}=Axoncurrent;
            %             figure(i)
            %             hold on
            %             plot(linspace(-5,5,length(current)),current)
            
            
            
            area{k,j,i}=sum(data_all{i}.areas);
        end
    end
end
%% Areas of axon compared to total
pUn=[];
pM=[];
for i=1:5
    for j=1:5
        pUn(i,j)=(area{1,2,(i-1)*5+j}-area{1,1,(i-1)*5+j})./area{1,2,(i-1)*5+j};
        pM(i,j)=(area{1,3,(i-1)*5+j}-area{1,1,(i-1)*5+j})./area{1,3,(i-1)*5+j};
    end
end
figure
boxplot(pUn')
axis([0 6 0 1])
figure
boxplot(pM')
axis([0 6 0 1])
%% Peak to peak voltages compared for NoAxon and Axon

for i=1:2
    for j=2:3
        figure
        data=zeros(5,5);
        for k=1:5
            for p=1:5
%                 data(k,p)=(VTraces{i,j,(k-1)*5+p}-VTraces{i,1,(k-1)*5+p})./VTraces{i,1,(k-1)*5+p}.*100;
                Axoned=(max(VTraces{i,j,(k-1)*5+p})-min(VTraces{i,j,(k-1)*5+p}));
                NoAxoned=(max(VTraces{i,1,(k-1)*5+p})-min(VTraces{i,1,(k-1)*5+p}));
                data(k,p)=((Axoned-NoAxoned))./(NoAxoned).*100;
            end
        end
        boxplot(data')
        axis([0 6 -275 275])
        fileName=['D:\CorticalNeuronDipole\Paper\Figures\DiffPPVoltage_' activationName{i} '_' AxonName{j} '.pdf'];
%             print(gcf,fileName,'-bestfit','-dpdf');
    end
end

%%
%% Peak to peak SomaCurrent compared for NoAxon and Axon

for i=1:2
    for j=2:3
        figure
        data=zeros(5,5);
        for k=1:5
            for p=1:5
                Axoned=(max(ITracesSoma{i,j,(k-1)*5+p})-min(ITracesSoma{i,j,(k-1)*5+p}));
                NoAxoned=(max(ITracesSoma{i,1,(k-1)*5+p})-min(ITracesSoma{i,1,(k-1)*5+p}));
                data(k,p)=((Axoned-NoAxoned))./(NoAxoned).*100;
            end
        end
        boxplot(data')
        axis([0 6 -60 80])
        fileName=['D:\CorticalNeuronDipole\Paper\Figures\DiffPPSomaCurrent_' activationName{i} '_' AxonName{j} '.pdf'];
%             print(gcf,fileName,'-bestfit','-dpdf');
    end
end
%% Peak to peak AxonCurrent compared for NoAxon and Axon

for i=1:2
    for j=2:3
        figure
        data=zeros(5,5);
        for k=1:5
            for p=1:5
                Axoned=(max(ITraces{i,j,(k-1)*5+p})-min(ITraces{i,j,(k-1)*5+p}));
                NoAxoned=(max(ITraces{i,1,(k-1)*5+p})-min(ITraces{i,1,(k-1)*5+p}));
                data(k,p)=((Axoned))%-NoAxoned))./(NoAxoned).*100;
            end
        end
        boxplot(data')
        axis([0 6 -50 200])
        fileName=['D:\CorticalNeuronDipole\Paper\Figures\DiffPPAxonCurrent_' activationName{i} '_' AxonName{j} '.pdf'];
%             print(gcf,fileName,'-bestfit','-dpdf');
    end
end
%%
for i=1:25
figure
plot(linspace(0,10,length(ITraces{1,1,i})),ITraces{1,1,i})
hold on
plot(linspace(0,10,length(ITraces{1,1,i})),ITraces{1,2,i})
plot(linspace(0,10,length(ITraces{1,1,i})),ITraces{1,3,i})
% legend('No Axon','Unmyelinated','Myelinated')
axis([0 10 -70 30])
fileName=['D:\CorticalNeuronDipole\Paper\Figures\AxonCurrentsWithAndWithoutAxon_CI_' num2str(i) '.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
    
end
%%
for i=1:25
figure
plot(linspace(0,10,length(ITraces{1,1,i})),ITracesSoma{2,1,i})
hold on
plot(linspace(0,10,length(ITraces{1,1,i})),ITracesSoma{2,2,i})
plot(linspace(0,10,length(ITraces{1,1,i})),ITracesSoma{2,3,i})
legend('No Axon','Unmyelinated','Myelinated')
fileName=['D:\CorticalNeuronDipole\Paper\Figures\SomaCurrentsWithAndWithoutAxon_CI_' num2str(i) '.pdf'];
axis([0 10 -140 40])
    print(gcf,fileName,'-bestfit','-dpdf');
end
%%
current=[current(:,1).*0;current(:,1)];
            where=find(max(abs(current))==abs(current));
            current = current((where-200):(where+200));
figure
plot(linspace(0,10,401),VTraces{1,1,17}(142:542))
hold on
plot(linspace(0,10,401),VTraces{1,2,17}(193:593))
plot(linspace(0,10,401),VTraces{1,3,17}(117:517))
legend('No Axon','Unmyelinated','Myelinated')
% fileName=['D:\CorticalNeuronDipole\Paper\Figures\RecordedVoltageWithAndWithoutAxon.pdf'];
%     print(gcf,fileName,'-bestfit','-dpdf');
