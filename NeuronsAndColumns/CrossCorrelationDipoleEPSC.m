%% Initialize
radii=[0.1 0.5 1 2 3 4 5 6 7 8 9 10 15 20];
dipolyness=zeros(25,length(radii)-1);
posz=zeros(25,length(radii)-1);
moment=zeros(25,length(radii)-1);
momentPosz=zeros(25,length(radii)-1);
directions=zeros(25,length(radii)-1);
%% Fits
tic
ActivationType={'SynapAct','CurrentInj'};
Folder = {'SynapticActivation','CurrentInj'};
AxonName = {'maxH_rax1','umaxH','maxH'};
activationName = {'synapact','currinjection'};
Axons={'NoAxon','Unmyel','Myel'};
for j=1:3
    for k=1%1:2
        load(['AllNeurons\' Folder{k} '\' AxonName{j} '_' activationName{k} '.mat'])
        for i=16:20%1:25
            load(['AllNeurons\' ActivationType{k} 'SphereVoltages\' Axons{j} '\Neuron_regBox' num2str(i) '_SphereVoltages_Iso_LowerMesh.mat']);
            dataOrig=scirunfield.field;% Load in Data for a given sphere
            load(['sphereLocs\SpherePointsMultiTrialHigherRes_' activationName{k} '_' AxonName{j} '_' num2str(i) '.mat']);
            locsOrig=scirunfield.node;
            if j==1
                radii=[0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 10 20]+max(data_all{i}.coordinates(:,2))./1000;
            else
                radii=[0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 10 20]+max(data_all{i}.coordinates(:,3))./1000;
            end
            for index=1:length(radii)-1
                which=radii(index).*10==round(sqrt(sum(locsOrig.^2)).*10);
                data=dataOrig(which);
                locs=locsOrig(:,which);
                radius=radii(index);% radius of sphere
                Xcoor=zeros(360,360);
                Moments=zeros(360,360);
                % Find timepoints we care about
                if j == 1
                    rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 10 0]).^2,2));
                else
                    rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 0 10]).^2,2));
                end
                current=(data_all{i}.i_mem_matrix.*data_all{i}.areas');
                current(:,1)=current(:,1)-data_all{i}.i_mem_matrix*data_all{i}.areas;
                current=current.*1e-2;%nA
                
                voltageTrace=current*rinv;
                voltageTrace = [voltageTrace.*0;voltageTrace];
                where = find(max(abs(voltageTrace))==abs(voltageTrace));
                current = [current.*0;current];
                % Locations of recording sites
                %                 index = 12;% 1cm away
                
                which=round(radii(index).*100)==round(sqrt(sum(locsOrig.^2)).*100);
                locs=locsOrig(:,which);
                
                % Neuron Voltages in Space
                current = current((where-100),:);
                coords = data_all{i}.coordinates./1000;
                data = zeros(1,length(locs));
                for q = 1:length(locs)
                    rinv = 1./sqrt(sum((coords-locs(:,q)').^2,2));
                    data(:,q)=data(:,q)+current*rinv./(4.*pi);
                end
                for theta=1:360
                    for phi=1:180
                        direction=[cosd(theta).*cosd(phi) sind(theta).*cosd(phi) sind(phi)];
                        angle=sum(direction.*locs',2)./(sqrt(sum(locs.^2))'.*sqrt(sum(direction.^2)));
                        dipole=angle./(4.*pi.*radius'.^2);
                        Moments(theta,phi)=dipole\data';
                        dipole=dipole'-mean(dipole);
                        Xcoor(theta,phi)=sum((data-mean(data)).*dipole)./(sqrt(sum(dipole.^2)).*sqrt(sum((data-mean(data)).^2)));

                    end
                end
                if j==1
                    momentPosz(i,index)=Moments(90,180);
                    posz(i,index)=Xcoor(90,180);% x direction oriented (No axon)
                else
                    momentPosz(i,index)=Moments(90,90);
                    posz(i,index)=Xcoor(90,90);% z direction oriented (With axon)
                end
                dipolyness(i,index)=max(max(abs(Xcoor)));
                dir=find(abs(Xcoor)==max(max(abs(Xcoor))));
                moment(i,index)=Moments(dir(1));
                [x,y]=ind2sub([360 360],dir(1));
                directions(i,index) = dir(1);
            end
            disp(num2str(i))
        end
        save(['AllNeurons\' ActivationType{k} 'SphereVoltages\DipolynessAndMomentAll_AllDir_' Axons{j} '_EPSC_l5_' Axons{j} '.mat'],'dipolyness','posz','moment','momentPosz','directions');
        toc
    end
end

%%
figure

for j=1:5
    data=[];
    for i=1:11
        data=[data saveDipoles{1,1,15+j,i}(100)];
    end
    semilogx([0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 10 20],abs(data),'linewidth',2)
    axis([0.01 10 0 1])
    xlabel('Distance From Top of Apical Dendrites (mm)')
    ylabel('Correlation (R)')
    hold on
end
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Dipolyness_EPSC_Distance_NoAxon.pdf'];
print(gcf,fileName,'-bestfit','-dpdf');