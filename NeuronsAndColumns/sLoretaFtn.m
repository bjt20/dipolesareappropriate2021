function sLoretaFtn(LeadField,phi,OutName)
%% Load in Lead-Field, Alpha, and recorded potentials
numContacts = length(LeadField(:,1));
numElem = length(LeadField(1,:))/3;

%% Create H matrix
RecordingSites = numContacts; % Number of recording sites
one = ones(RecordingSites,1); % Ones vector
H = eye(RecordingSites)-one*one'./RecordingSites; % Centering Matrix

%% Regularize Lead-Field and Recordings
phi = H*phi;%./norm(phi); 
% Power=sqrt(sum(reshape(LeadField,[3*numContacts,length(LeadField)/3]).^2));
% Power=[Power;Power;Power];
K = H*LeadField;%./(Power(:)');

% [U,s,V] = csvd(K);
% Alp = discrep4(U,s,V,phi,norm(phi));

% order=floor(log(abs(max(max(K*K'))))./log(10))+1;
alpha=0;%Alp^2;%10^order;% constant regulatization parameter >=0

% clear LeadField %Free up memory
%% Make T matrix = K'H[HKK'H+aH]+
T=K'*pinv(K*K'+alpha.*H); % make T matrix #rec x #dipoles

%% Calculate Jhat
Jhat = T*phi;
%% Loop through each element to find Localization inference map
LIM=zeros(1,floor(length(K)/3));
for i=1:length(K)/3
    index=(3*(i-1)+1); 
    dipole = Jhat(index:index+2,1);
    covar=T(index:index+2,:)*K(:,index:index+2);
    LIM(i)=dipole'/covar*dipole;
end
% scirunmatrix=zeros(numElem,1);
scirunmatrix=LIM';
% save(OutName,'scirunmatrix')