%% Plot Voltages in Time
load AllNeurons\DipoleMomentsAndDipolyness.mat
radii=[0.05 0.1 0.2 0.3 0.4 0.5 1 2 3 4 5 6 7 8 9 10 15 20];
for i=1:2
    for k=1:25
        figure
        for j=1:3
            hold on
            plot(linspace(-5,5,length(saveVoltages{1,1,1,1})),saveVoltages{i,j,k,16}.*1000,'LineWidth',2)
            xlabel('Time (ms)')
            ylabel('Voltage (nV)')
            axis([-5 5 -15 35])
            xticks([-5 0 5])
            yticks([-15 0 35])
        end
        fileName=['Figures\Vm_AllNeurons_' ActivationType{i} '_' num2str(k) '.pdf'];
        print(gcf,fileName,'-bestfit','-dpdf');
    end
end
%% Plot Voltages in Space
radii=[0.1 0.5 1 2 3 4 5 6 7 8 9 10 15];
ActivationType={'CurrentInj','SynapAct'};
AxonType={'NoAxon','Unmyel','Myel'};
for i=1:2
    for j=1:3
        load(['AllNeurons\' ActivationType{i} 'SphereVoltages\DipolynessAndMomentAll_AllDir_' AxonType{j} '.mat'])
        figure
        for k=1:5
            for p=1:5
                hold on
                if k==1
                    plot(radii,abs(posz((k-1).*5+p,:)),'Color',[0, 0.4470, 0.7410],'LineWidth',2)
                elseif k==2
                    plot(radii,abs(posz((k-1).*5+p,:)),'Color',[0.8500, 0.3250, 0.0980],'LineWidth',2)
                elseif k==3
                    plot(radii,abs(posz((k-1).*5+p,:)),'Color',[0.9290, 0.6940, 0.1250] 	,'LineWidth',2)
                elseif k==4
                    plot(radii,abs(posz((k-1).*5+p,:)),'Color',[0.4940, 0.1840, 0.5560],'LineWidth',2)
                else
                    plot(radii,abs(posz((k-1).*5+p,:)),'Color',[0.4660, 0.6740, 0.1880],'LineWidth',2)
                end
            end
        end
        xlabel('Distance(mm)')
        ylabel('Coefficient of Correlation (R)')
        axis([0.1 10 0 1])
        set(gca, 'XScale', 'log')
        fileName=['Figures\DipoleSpace_' ActivationType{i} '_' AxonType{j} '_AllNeurons.pdf'];
        print(gcf,fileName,'-bestfit','-dpdf');
    end
end
%% Plot Voltages in Space best direction
radii=[0.1 0.5 1 2 3 4 5 6 7 8 9 10 15];
ActivationType={'CurrentInj','SynapAct'};
AxonType={'NoAxon','Unmyel','Myel'};
for i=1:2
    for j=1:3
        load(['AllNeurons\' ActivationType{i} 'SphereVoltages\DipolynessAndMomentAll_AllDir_' AxonType{j} '.mat'])
        figure
        for k=1:5
            for p=1:5
                hold on
                if k==1
                    plot(radii,abs(dipolyness((k-1).*5+p,:)),'Color',[0, 0.4470, 0.7410],'LineWidth',2)
                elseif k==2
                    plot(radii,abs(dipolyness((k-1).*5+p,:)),'Color',[0.8500, 0.3250, 0.0980],'LineWidth',2)
                elseif k==3
                    plot(radii,abs(dipolyness((k-1).*5+p,:)),'Color',[0.9290, 0.6940, 0.1250] 	,'LineWidth',2)
                elseif k==4
                    plot(radii,abs(dipolyness((k-1).*5+p,:)),'Color',[0.4940, 0.1840, 0.5560],'LineWidth',2)
                else
                    plot(radii,abs(dipolyness((k-1).*5+p,:)),'Color',[0.4660, 0.6740, 0.1880],'LineWidth',2)
                end
            end
        end
        xlabel('Distance(mm)')
        ylabel('Coefficient of Correlation (R)')
        axis([0.1 10 0 1])
        set(gca, 'XScale', 'log')
        fileName=['Figures\DipoleSpace_' ActivationType{i} '_' AxonType{j} '_AllNeurons_Best.pdf'];
        print(gcf,fileName,'-bestfit','-dpdf');
    end
end
%% Plot Voltages in Space errorbars
radii=[0.1 0.5 1 2 3 4 5 6 7 8 9 10 15];
ActivationType={'CurrentInj','SynapAct'};
AxonType={'NoAxon','Unmyel','Myel'};
for i=1:2
    for j=1:3
        load(['AllNeurons\' ActivationType{i} 'SphereVoltages\DipolynessAndMomentAll_AllDir_' AxonType{j} '.mat'])
        figure
        for k=1:5
            errorbar(radii,mean(abs(posz(((k-1).*5+1):(k.*5),:))),std(abs(posz(((k-1).*5+1):(k.*5),:)))./sqrt(5),'LineWidth',2)
            hold on
        end
        xlabel('Distance(mm)')
        ylabel('Coefficient of Correlation (R)')
        axis([0.1 10 0 1])
        set(gca, 'XScale', 'log')
        fileName=['Figures\DipoleSpace_' ActivationType{i} '_' AxonType{j} '.pdf'];
        print(gcf,fileName,'-bestfit','-dpdf');
    end
end
%% Plot Voltages in Space errorbars best direction
radii=[0.1 0.5 1 2 3 4 5 6 7 8 9 10 15];
ActivationType={'CurrentInj','SynapAct'};
AxonType={'NoAxon','Unmyel','Myel'};
for i=1:2
    for j=1:3
        load(['AllNeurons\' ActivationType{i} 'SphereVoltages\DipolynessAndMomentAll_AllDir_' AxonType{j} '.mat'])
        figure
        for k=1:5
            errorbar(radii,mean(abs(dipolyness(((k-1).*5+1):(k.*5),:))),std(abs(dipolyness(((k-1).*5+1):(k.*5),:)))./sqrt(5),'LineWidth',2)
            hold on
        end
        xlabel('Distance(mm)')
        ylabel('Coefficient of Correlation (R)')
        axis([0.1 10 0 1])
        set(gca, 'XScale', 'log')
        fileName=['Figures\DipoleSpace_' ActivationType{i} '_' AxonType{j} '_Best.pdf'];
        print(gcf,fileName,'-bestfit','-dpdf');
    end
end