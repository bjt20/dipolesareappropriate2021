%% Initialize
radii=[0.1 0.5 1 2 3 4 5 6 7 8 9 10 15 20];
dipolyness=zeros(25,length(radii)-1);
posz=zeros(25,length(radii)-1);
directions=[];
moment=zeros(25,length(radii)-1);
momentPosz=zeros(25,length(radii)-1);
%% Fits
Folder = {'SynapAct','CurrentInj'};
AxonName = {'maxH_rax1','umaxH','maxH'};
activationName = {'synapact','currinjection'};
Axons={'NoAxon','Unmyel','Myel'};
VTraces={};
load('SpherePointsMultiTrialHigherRes.mat');
locsOrig=scirunfield.node;
saveMoments={};
saveDipoles={};
saveVoltages={};
tic
for k = 1:2 % activation type
    for j = 1:3 % axon types
        load(['AllNeurons\' Folder{k} '\' AxonName{j} '_' activationName{k} '.mat'])
        for i=1:25 % Axon number
            radii=[0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 10 20];
            
            load(['AllNeurons\' Folder{k} 'SphereVoltages\DipolynessAndMomentAll_AllDir_' Axons{j} '.mat'])
            for index = 1:length(radii)
                % Find timepoints we care about
                if j == 1
                    rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 10 0]).^2,2));
                else
                    rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 0 10]).^2,2));
                end
                current=(data_all{i}.i_mem_matrix.*data_all{i}.areas');
                current(:,1)=current(:,1)-data_all{i}.i_mem_matrix*data_all{i}.areas;
                current=current.*1e-2;%nA
                
                voltageTrace=current*rinv;
                voltageTrace = [voltageTrace.*0;voltageTrace];
                where = find(max(abs(voltageTrace))==abs(voltageTrace));
                current = [current.*0;current];
                % Locations of recording sites
                %                 index = 12;% 1cm away
                
                which=round(radii(index).*100)==round(sqrt(sum(locsOrig.^2)).*100);
                %             data=dataOrig(which);
                locs=locsOrig(:,which);
                
                % Neuron Voltages in Space
                current = current((where-200):(where+200),:);
                coords = data_all{i}.coordinates./1000;
                data = zeros(401,length(locs));
                for q = 1:length(locs)
                    rinv = 1./sqrt(sum((coords-locs(:,q)').^2,2));
                    data(:,q)=data(:,q)+current*rinv./(4.*pi);
                end
                % Dipole Voltages in Space
                radius=radii(index);% radius of sphere
                theta=90;
                if j==1 % No Axon
                    phi=180;
                else % With Axon
                    phi=90;
                end
                direction=[cosd(theta).*cosd(phi) sind(theta).*cosd(phi) sind(phi)];
                angle=sum(direction.*locs',2)./(sqrt(sum(locs.^2))'.*sqrt(sum(direction.^2)));
                dipole=angle./(4.*pi.*radius'.^2);
                dipole=dipole'-mean(dipole);
                Xcoor = zeros(1,401);
                Moments = zeros(1,401);
                for t=1:401
                    if sum(data(t,:))~=0
                        Xcoor(t)=sum((data(t,:)-mean(data(t,:))).*dipole)./(sqrt(sum(dipole.^2)).*sqrt(sum((data(t,:)-mean(data(t,:))).^2)));
                        Moments(t)=dipole'\data(t,:)';
                    end
                end
                saveMoments{k,j,i,index}=Moments;
                saveDipoles{k,j,i,index}=Xcoor;
                if j == 1
                    rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 radii(index) 0]).^2,2));
                else
                    rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 0 radii(index)]).^2,2));
                end
                current=(data_all{i}.i_mem_matrix.*data_all{i}.areas');
                current(:,1)=current(:,1)-data_all{i}.i_mem_matrix*data_all{i}.areas;
                current=current.*1e-2;%nA
                current = [current.*0;current];
                voltageTrace=current((where-200):(where+200),:)*rinv;
                saveVoltages{k,j,i,index}=voltageTrace;
            end
        end
    end
end
toc

save AllNeurons\DipoleMomentsAndDipolynessUp.mat saveMoments saveDipoles saveVoltages
