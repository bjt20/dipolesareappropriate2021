%% Load data
% load coordinates
x=load('Cell_Coordinates\x.dat');
y=load('Cell_Coordinates\y.dat');
z=load('Cell_Coordinates\z.dat');
x_coord=[];
y_coord=[];
z_coord=[];
%% load i_membrane recordings from each compartment
currents=[];
for i=1:336
    tempdata=load(['TransCurrents_Column_Model\lfp' num2str(i) '.dat']);
    time=tempdata(:,1);
    %     tempdata=floor(tempdata.*1e12);
    tempdata=tempdata(:,2:end)-mean(tempdata(:,2:end),2);
    %     off=sum(tempdata,2);
    %     tempdata()
    
    currents=[currents;tempdata(:,:)'];
    x_coord=[x_coord x(i+1,1:length(tempdata(1,:)))];
    y_coord=[y_coord z(i+1,1:length(tempdata(1,:)))];
    z_coord=[z_coord y(i+1,1:length(tempdata(1,:)))];
end

%%
disp('New')
dist=1500;%(�m)
pe = [0 0 10000]; % electrode position (�m)
titles={'+x','-x','+y','-y','+z','-z'};
sum_ve=0;

sum_ve=0;
sigma_e = 0.2e-6; % S/um
r = sqrt(((x_coord'- pe(1)').^2+(y_coord'-pe(2)').^2+(z_coord'- pe(3)').^2)); % �m
ve = (currents'*(1./r))./(4*pi*sigma_e)*1e6; %[mA * 1/�m]/(S/um) *1e6-> nV

figure
plot(time,ve);
axis square
ylabel('Recorded Voltage (nV)')
xlabel('Time (ms)')
fileName=['D:\CorticalNeuronDipole\Paper\Figures\CorticalColumnVm_2.pdf'];
print(gcf,fileName,'-bestfit','-dpdf');
%%
radii=[0.05 0.1 0.2 0.3 0.4 0.5 1 2 3 4 5 6 7 8 9 10 15 20].*1000;
dipolyness=zeros(25,length(radii)-1);
posz=zeros(25,length(radii)-1);
directions=[];
moment=zeros(25,length(radii)-1);
momentPosz=zeros(25,length(radii)-1);
VTraces={};
load('SpherePointsMultiTrialHigherRes.mat');
locsOrig=scirunfield.node;
saveMoments={};
saveDipoles={};
tic

% for t = 1:length(time)
for index = length(radii)
    % Neuron Voltages in Space
    which=radii(index).*100./1000==round(sqrt(sum(locsOrig.^2)).*100);
    %             data=dataOrig(which);
    locs=locsOrig(:,which).*1000;
    coords = [x_coord+4.2392;y_coord-11.9974;z_coord+306.2578];
    data = zeros(length(time),length(locs));
    for q = 1:length(locs)
        rinv = 1./sqrt(sum((coords'-locs(:,q)').^2,2));
        data(:,q)=data(:,q)+currents'*rinv./(4.*pi);
    end
    % Dipole Voltages in Space
    radius=radii(index);% radius of sphere
    theta=90;
%     phi=180;
    phi=90;
    direction=[cosd(theta).*cosd(phi) sind(theta).*cosd(phi) sind(phi)];
    angle=sum(direction.*locs',2)./(sqrt(sum(locs.^2))'.*sqrt(sum(direction.^2)));
    dipole=angle./(4.*pi.*radius'.^2);
    dipole=dipole'-mean(dipole);
    Xcoor = zeros(1,length(time));
    Moments = zeros(1,length(time));
    for t=1:length(time)
        if sum(data(t,:))~=0
            %                         Xcoor(t)=sum((data(t,:)-mean(data(t,:))).*dipole)./(sqrt(sum(dipole.^2)).*sqrt(sum((data(t,:)-mean(data(t,:))).^2)));
            Xcoor(t)=corr(data(t,:)',dipole');    
%             corr(data(t,:)',dipole')
            Moments(t)=dipole'\data(t,:)';
        end
    end
    saveMoments{index}=Moments;
    saveDipoles{index}=Xcoor;
end
toc
% end
%%
figure
% plot(saveMoments{16})
% hold on
plot(time,saveDipoles{18})
times=[44 598 1146 1693 2227 2970 3850 4361 4863 5635 6431 6967 7499 8072 8900 9649 102 2287 4415 6008];
hold on
plot(time(times),1.2*ones(length(times),1),'r*')
xlabel('Time (ms)')
ylabel('Correlation With Ideal Dipole (R)')
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\CorticalColumnDipoleTime_WithDots.pdf'];
print(gcf,fileName,'-bestfit','-dpdf');

% figure
% plot(ve./40)
data=[];

for i=1:length(radii)
    data=[data saveDipoles{i}(5951)];
end
figure
plot(radii./1000,data)
axis([0 10 0 1])
xlabel('Recording Distance (mm)')
ylabel('Correlation with Ideal Dipole (R)')
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\CorticalColumnDipoleDistance_2.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf');
%%
peaks=[270 595 1142 1689 2227 2967 3318 3846 4360 4863 5635 5941 6431 6963 7196 7496 7826 8072 8896 9155];
figure
plot(saveMoments{18}(peaks).*1000,saveDipoles{18}(peaks),'k*')
xlabel('Dipole Moment (pA-m)')
ylabel('Correlation With Ideal Dipole')