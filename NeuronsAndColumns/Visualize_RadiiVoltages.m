Folder = {'SynapticActivation','CurrentInj'};
AxonName = {'maxH_rax1','umaxH','maxH'};
activationName = {'synapact','currinjection'};
rad=[1].*1000;

k = 2 % activation type
j = 2 % axon types
load(['AllNeurons\' Folder{k} '\' AxonName{j} '_' activationName{k} '.mat'])
i=17 % Axon number
% figure
% plot3(data_all{i}.coordinates(:,1)./1000,data_all{i}.coordinates(:,2)./1000.*0,data_all{i}.coordinates(:,3)./1000,'k*')
cd plot_cell_data
cell_id = 16; 
nrn_model_ver = 'maxH'; % only one included in local cell_data/
% Plot settings
lw = 2; % line width
% Load cell_data: coordinates and connectivity of compartments (parent_inds)
cell_data = loadCellData(cell_id,nrn_model_ver); % use default data_files
% Plot
figure; 
% plotCellLines('cell_data',cell_data,'lw',lw); 
cd ..
for r=rad
    theta=0:360;
    X=r.*cosd(theta);
    Y=0.*X;
    Z=r.*sind(theta);
    locs=[X;Y;Z];
    rinv=1./sqrt(sum((data_all{i}.coordinates-[0 0 10]).^2,2));
    current=(data_all{i}.i_mem_matrix.*data_all{i}.areas');
    current(:,1)=current(:,1)-data_all{i}.i_mem_matrix*data_all{i}.areas;
    current=current.*1e-2;%nA
    voltageTrace=current*rinv;
    where = find(max((voltageTrace))==(voltageTrace));
    data=zeros(length(current),length(locs));
    for q = 1:length(locs)
        rinv = 1./sqrt(sum((data_all{i}.coordinates-locs(:,q)').^2,2));
        data(:,q)=data(:,q)+current*rinv./(4.*pi);
    end
    hold on
    col = data(87,:)./max(abs(data(87,:)));  % This is the color, vary with x in this case.
    surface([X;X],[Y;Y],[Z;Z],[col;col],...
        'facecol','no',...
        'edgecol','interp',...
        'linew',2);
end
axis square
axis equal
colormap jet
colorbar
view([180 0])
caxis([-1 1])
fileName=['Figures\Vm_normalized_circle.pdf'];
print(gcf,fileName,'-bestfit','-dpdf');