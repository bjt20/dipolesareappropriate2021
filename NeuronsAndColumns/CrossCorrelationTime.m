%% Load data into data_all
% load AllNeurons\SynapticActivation\umaxH_synapact.mat
% load AllNeurons\SynapticActivation\maxH_rax1_synapact.mat
% load AllNeurons\SynapticActivation\maxH_synapact.mat
% load AllNeurons\CurrentInj\umaxH_currinjection.mat
% load AllNeurons\CurrentInj\maxH_rax1_currinjection.mat
% load AllNeurons\CurrentInj\maxH_currinjection.mat
% scirunfield=struct;
%%
Folder = {'SynapAct','CurrentInj'};
AxonName = {'maxH_rax1','umaxH','maxH'};
activationName = {'synapact','currinjection'};
VTraces={};
for k = 1:2 % activation type
    for j = 1:3 % axon types
        load(['AllNeurons\' Folder{k} '\' AxonName{j} '_' activationName{k} '.mat'])
        for i=1:25 % Axon number
            if j == 1
                rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 10 0]).^2,2));
            else
                rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 0 10]).^2,2));
            end
            current=(data_all{i}.i_mem_matrix.*data_all{i}.areas');
            current(:,1)=current(:,1)-data_all{i}.i_mem_matrix*data_all{i}.areas;
            current=current.*1e-2;%nA
            voltageTrace=current*rinv;
            voltageTrace = [voltageTrace.*0;voltageTrace];
            where = find(max((voltageTrace))==(voltageTrace));
            voltageTrace = voltageTrace((where-200):(where+200));
            VTraces{k,j,i}=voltageTrace;
        end
    end
end
%%
% crossCorrs=zeros(150,150);
crossCorrs=[];
ct1=1;
for i = 1:2
    for j = 1:3
        for k = 1:25
%             ct2=1;
            for ii=2
                %                 for jj = 1:3
                for kk = 1:25
                    r = xcorr(VTraces{i,j,k}-mean(VTraces{i,j,k}),VTraces{i,j,kk}-mean(VTraces{i,j,kk}),'coef');
                    %                         ind=find(abs(r)==max(abs(r)));
                    ind=find((r)==max((r)));
                    crossCorrs(i,j,k,kk) = r(ind);
%                     ct2=ct2+1;
                    %                         if kk==25
                    %                             ct2=ct2+1;
                    %                         end
                    %                     end
                    %                 end
                end
%                 ct1=ct1+1;
                %             if k==25
                %                 ct1=ct1+1;
            end
        end
    end
end
figure
imagesc(squeeze(crossCorrs(1,3,:,:)))
axis square
colorbar
caxis([0 1])
%% CI vs SYNACT
crossCorrs=[];
pchange=[];
ct1=1;
for i = 1
    for j = 1:3
        for k = 1:25
            for ii=2
                r = xcorr(VTraces{i,j,k}-mean(VTraces{i,j,k}),VTraces{ii,j,k}-mean(VTraces{ii,j,k}),'coef');
                ind=find((r)==max((r)));
                pchange=[pchange ((max(VTraces{i,j,k})-min(VTraces{i,j,k}))-(max(VTraces{ii,j,k})-min(VTraces{ii,j,k})))./(max(VTraces{ii,j,k})-min(VTraces{ii,j,k})).*100];
                crossCorrs = [crossCorrs r(ind)];
            end
        end
    end
end
data=reshape(crossCorrs,25,3);
data=reshape(data',15,5);
data2=reshape(pchange,25,3);
data2=reshape(data2',15,5);

figure
boxplot(data)
axis([0 6 0 1])
ylabel('Coefficient of Correlation (R)')
fileName=['D:\CorticalNeuronDipole\Paper\Figures\xcoor_CIvsSA_By_Layer.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
figure
boxplot(data2)
hold on
plot([0 6],[0 0],'k--')
plot([1 2],[-1.1364 -14.1089],'k+')
ylabel('% Difference')
axis([0 6 -110 110])
fileName=['D:\CorticalNeuronDipole\Paper\Figures\pdiff_CIvsSA_By_Layer.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
    
figure
plot(linspace(0,10,401),VTraces{1,3,12})
hold on
plot(linspace(0,10,401),VTraces{2,3,12})
fileName=['D:\CorticalNeuronDipole\Paper\Figures\xcorrTime_Example_Bad.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
    
figure
plot(linspace(0,10,401),VTraces{1,2,6})
hold on
plot(linspace(0,10,401),VTraces{2,2,6})
fileName=['D:\CorticalNeuronDipole\Paper\Figures\xcorrTime_Example_okay.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');

figure
plot(linspace(0,10,401),VTraces{1,3,5})
hold on
plot(linspace(0,10,401),VTraces{2,3,5})
fileName=['D:\CorticalNeuronDipole\Paper\Figures\xcorrTime_Example_good.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
    
%% Axon Type no axon to unmyel
crossCorrs=[];
ct1=1;
for i = 1:2
    for j = 1
        for k = 1:25
            for jj=2
                r = xcorr(VTraces{i,j,k}-mean(VTraces{i,j,k}),VTraces{i,jj,k}-mean(VTraces{i,jj,k}),'coef');
                ind=find((r)==max((r)));
                crossCorrs = [crossCorrs r(ind)];
            end
        end
    end
end
data=reshape(crossCorrs,25,2);
data=reshape(data',10,5);
figure
boxplot(data)
hold on
plot([3 4],[crossCorrs(39) crossCorrs(42)],'k+')
axis([0 6 0 1])
fileName=['D:\CorticalNeuronDipole\Paper\Figures\xcoor_NoAxvsUnmyel_By_Layer.pdf'];
%     print(gcf,fileName,'-bestfit','-dpdf');
%% Axon Type no axon to myel
crossCorrs=[];
ct1=1;
for i = 1:2
    for j = 1
        for k = 1:25
            for jj=3
                r = xcorr(VTraces{i,j,k}-mean(VTraces{i,j,k}),VTraces{i,jj,k}-mean(VTraces{i,jj,k}),'coef');
                ind=find((r)==max((r)));
                crossCorrs = [crossCorrs r(ind)];
            end
        end
    end
end
data=reshape(crossCorrs,25,2);
data=reshape(data',10,5);
figure
boxplot(data)
hold on
plot([3 4],[crossCorrs(39) crossCorrs(42)],'k+')
axis([0 6 0 1])
fileName=['D:\CorticalNeuronDipole\Paper\Figures\xcoor_NoAxvsMyel_By_Layer.pdf'];
%     print(gcf,fileName,'-bestfit','-dpdf');

%% Axon Type no axon to myel
crossCorrs=[];
rms=[];
ct1=1;
for i = 1:2
    for j = 1
        for k = 1:25
            for jj=2
                r = xcorr(VTraces{i,j,k}-mean(VTraces{i,j,k}),VTraces{i,jj,k}-mean(VTraces{i,jj,k}),'coef');
                y=((VTraces{i,j,k}-mean(VTraces{i,j,k}))-(VTraces{i,jj,k}-mean(VTraces{i,jj,k})));
                rmsErr=sqrt(sum(((VTraces{i,j,k}-mean(VTraces{i,j,k}))-(VTraces{i,jj,k}-mean(VTraces{i,jj,k}))).^2))./(mean(y));
                ind=find((r)==max((r)));
                crossCorrs = [crossCorrs r(ind)];
                rms=[rms rmsErr];
            end
        end
    end
end
data=reshape(crossCorrs,25,2);
data=reshape(data',10,5);
data2=reshape(rms,25,2);
data2=reshape(data2',10,5);

figure
boxplot(data)
hold on
plot([3 4],[crossCorrs(39) crossCorrs(42)],'k+')
axis([0 6 0 1])
fileName=['D:\CorticalNeuronDipole\Paper\Figures\xcoor_UnmyelvsMyel_By_Layer.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');

% figure
% boxplot(data2)

%%
figure
data={};
% Compares NoAxon Synaptic Actiation to others with one same
data{1}=crossCorrs(1:25,1:25);
data{2}=crossCorrs(1:25,26:50);
data{3}=crossCorrs(1:25,51:75);
data{4}=crossCorrs(1:25,76:100);
% Compares Unmyel synaptic activation to others
data{5}=crossCorrs(26:50,1:25);
data{6}=crossCorrs(26:50,26:50);
data{7}=crossCorrs(26:50,51:75);
data{8}=crossCorrs(26:50,101:125);
% Compares Myel synaptic activation to others
data{9}=crossCorrs(51:75,1:25);
data{10}=crossCorrs(51:75,26:50);
data{11}=crossCorrs(51:75,51:75);
data{12}=crossCorrs(51:75,126:150);

% Compares NoAxon CurrInj to others with one same
data{13}=crossCorrs(76:100,76:100);
data{14}=crossCorrs(76:100,101:125);
data{15}=crossCorrs(76:100,126:150);
data{16}=crossCorrs(76:100,1:25);
% Compares Unmyel currentInj to others
data{17}=crossCorrs(101:125,76:100);
data{18}=crossCorrs(101:125,101:125);
data{19}=crossCorrs(101:125,126:150);
data{20}=crossCorrs(101:125,26:50);
% Compares Myel currentInj to others
data{21}=crossCorrs(126:150,76:100);
data{22}=crossCorrs(126:150,101:125);
data{23}=crossCorrs(126:150,126:150);
data{24}=crossCorrs(126:150,51:75);
for i=1:24
    subplot(6,4,i)
    imagesc(data{i})
end
means=[];
stds=[];

for i=1:2
    means((i-1)*9+1)=mean(mean(data{(i-1)*12+1}-data{(i-1)*12+2}));
    stds((i-1)*9+1)=std(std(data{(i-1)*12+1}-data{(i-1)*12+2}))./sqrt(25*25);
    means((i-1)*9+2)=mean(mean(data{(i-1)*12+1}-data{(i-1)*12+3}));
    stds((i-1)*9+2)=std(std(data{(i-1)*12+1}-data{(i-1)*12+3}))./sqrt(25*25);
    means((i-1)*9+3)=mean(mean(data{(i-1)*12+1}-data{(i-1)*12+4}));
    stds((i-1)*9+3)=std(std(data{(i-1)*12+1}-data{(i-1)*12+4}))./sqrt(25*25);
    
    means((i-1)*9+4)=mean(mean(data{(i-1)*12+6}-data{(i-1)*12+5}));
    stds((i-1)*9+4)=std(std(data{(i-1)*12+6}-data{(i-1)*12+5}))./sqrt(25*25);
    means((i-1)*9+5)=mean(mean(data{(i-1)*12+6}-data{(i-1)*12+7}));
    stds((i-1)*9+5)=std(std(data{(i-1)*12+6}-data{(i-1)*12+7}))./sqrt(25*25);
    means((i-1)*9+6)=mean(mean(data{(i-1)*12+6}-data{(i-1)*12+8}));
    stds((i-1)*9+6)=std(std(data{(i-1)*12+6}-data{(i-1)*12+8}))./sqrt(25*25);
    
    means((i-1)*9+7)=mean(mean(data{(i-1)*12+11}-data{(i-1)*12+9}));
    stds((i-1)*9+7)=std(std(data{(i-1)*12+11}-data{(i-1)*12+9}))./sqrt(25*25);
    means((i-1)*9+8)=mean(mean(data{(i-1)*12+11}-data{(i-1)*12+10}));
    stds((i-1)*9+8)=std(std(data{(i-1)*12+11}-data{(i-1)*12+10}))./sqrt(25*25);
    means((i-1)*9+9)=mean(mean(data{(i-1)*12+11}-data{(i-1)*12+12}));
    stds((i-1)*9+9)=std(std(data{(i-1)*12+11}-data{(i-1)*12+12}))./sqrt(25*25);
end
ts = tinv([0.0014  0.9986],624);      % T-Score
CI = means + ts'*stds
%%

for i=1:24
    temp=[];
    for j=1:25
        d=data{i};
        temp=[temp d(j,j)];
    end
    use{i}=temp;
end
means=[];
stds=[];
for i=1:24
    means=[means mean(use{i})];
    stds=[stds std(use{i})./sqrt(25)];
end
ts = tinv([0.0014  0.9986],24);      % T-Score
CI = means + ts'*stds
%%
savedata=[];
axontype=[];
stimtype=[];
axontype2=[];
stimtype2=[];
layer1=[];
layer2=[];
indi=[];
indj=[];
for i=0:149
    for j= 0:149
        indi=[indi,i];
        indj=[indj j];
        savedata=[savedata;crossCorrs(i+1,j+1)];
        if j<75
            stimtype=[stimtype;0];
        else
            stimtype=[stimtype;1];
        end
        if mod(j,75)<25
            axontype=[axontype;0];
        elseif mod(j,75)<50
            axontype=[axontype;1];
        else
            axontype=[axontype;2];
        end
        if i<75
            stimtype2=[stimtype2;0];
        else
            stimtype2=[stimtype2;1];
        end
        if mod(i,75)<25
            axontype2=[axontype2;0];
        elseif mod(i,75)<50
            axontype2=[axontype2;1];
        else
            axontype2=[axontype2;2];
        end
        layer1 = [layer1;mod(j,25)];
        layer2 = [layer2;mod(i,25)];
        %         if mod(j,25)<5
        %             layer1=[layer1;1];
        %         elseif mod(j,25)<10
        %             layer1=[layer1;2];
        %         elseif mod(j,25)<15
        %             layer1=[layer1;4];
        %         elseif mod(j,25)<20
        %             layer1=[layer1;5];
        %         else
        %             layer1=[layer1;6];
        %         end
        %         if mod(i,25)<5
        %             layer2=[layer2;1];
        %         elseif mod(i,25)<10
        %             layer2=[layer2;2];
        %         elseif mod(i,25)<15
        %             layer2=[layer2;4];
        %         elseif mod(i,25)<20
        %             layer2=[layer2;5];
        %         else
        %             layer2=[layer2;6];
        %         end
    end
end
c=layer1==layer2; %& indi'~=indj';
a=stimtype(c);
b=axontype(c);
d=layer1(c);
e=savedata(c);
f=stimtype2(c);
g=axontype2(c);
% c=e~=1;
% a=a(c);
% b=b(c);
% d=d(c);
% e=e(c);
% f=f(c);
% g=g(c);
%%
figure
imagesc(crossCorrs(1:5,1:5))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer1CrossCorr.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
    
figure
imagesc(crossCorrs(6:10,6:10))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer23CrossCorr.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');

figure
imagesc(crossCorrs(11:15,11:15))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer4CrossCorr.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');

figure
imagesc(crossCorrs(16:20,16:20))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer5CrossCorr.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');

figure
imagesc(crossCorrs(21:25,21:25))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer6CrossCorr.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
%%
figure
imagesc(squeeze(crossCorrs(2,2,1:5,1:5)))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer1CrossCorrUnmyel.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
    
figure
imagesc(squeeze(crossCorrs(2,2,6:10,6:10)))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer23CrossCorrUnmyel.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');

figure
imagesc(squeeze(crossCorrs(2,2,11:15,11:15)))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer4CrossCorrUnmyel.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');

figure
imagesc(squeeze(crossCorrs(2,2,16:20,16:20)))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer5CrossCorrUnmyel.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');

figure
imagesc(squeeze(crossCorrs(2,2,21:25,21:25)))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer6CrossCorrUnmyel.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
%%
figure
imagesc(squeeze(crossCorrs(2,3,1:5,1:5)))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer1CrossCorrMyel.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
    
figure
imagesc(squeeze(crossCorrs(2,3,6:10,6:10)))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer23CrossCorrMyel.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');

figure
imagesc(squeeze(crossCorrs(2,3,11:15,11:15)))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer4CrossCorrMyel.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');

figure
imagesc(squeeze(crossCorrs(2,3,16:20,16:20)))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer5CrossCorrMyel.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');

figure
imagesc(squeeze(crossCorrs(2,3,21:25,21:25)))
caxis([0,1])
% caxis([-1,1])
axis off
axis square
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Layer6CrossCorrMyel.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
%%
figure
plot(linspace(0,10,length(VTraces{2,1,17})),VTraces{2,1,19},'LineWidth',2)
hold on
plot(linspace(0,10,length(VTraces{2,2,17})),VTraces{2,2,19},'LineWidth',2)
plot(linspace(0,10,length(VTraces{2,3,17})),VTraces{2,3,19},'LineWidth',2)
legend('No Axon','Unmyelinated','Myelinated')
fileName=['D:\CorticalNeuronDipole\Paper\Figures\GreatCrossCorr.pdf'];
%     print(gcf,fileName,'-bestfit','-dpdf');
    
figure
plot(linspace(0,10,length(VTraces{2,1,13})),VTraces{2,1,14},'LineWidth',2)
hold on
plot(linspace(0,10,length(VTraces{2,2,13})),VTraces{2,2,14},'LineWidth',2)
plot(linspace(0,10,length(VTraces{2,3,13})),VTraces{2,3,14},'LineWidth',2)
fileName=['D:\CorticalNeuronDipole\Paper\Figures\BadCrossCorr.pdf'];
%     print(gcf,fileName,'-bestfit','-dpdf');
%%
figure
plot(linspace(0,10,length(VTraces{2,1,16})),VTraces{2,1,15},'LineWidth',2)
hold on
plot(linspace(0,10,length(VTraces{2,2,16})),VTraces{2,2,15},'LineWidth',2)
plot(linspace(0,10,length(VTraces{2,3,16})),VTraces{2,3,15},'LineWidth',2)
%%
% c=layer1==layer2; %& indi'~=indj';
% a=stimtype(c);
% b=axontype(c);
% d=layer1(c);
% e=savedata(c);
% f=stimtype2(c);
% g=axontype2(c);
figure
axonData=[];
for i=0:8
    axonData=[axonData e(b.*3+g==i)];
end
% bar(mean(axonData(:,[1 2 3 5 6 9])))
% hold on
% sem=std(axonData(:,[1 2 3 5 6 9]))./10;
% er = errorbar([1 2 3 4 5 6],mean(axonData(:,[1 2 3 5 6 9])),sem);    
% er.Color = [0 0 0];                            
% er.LineStyle = 'none';
boxplot(axonData(:,[1 2 3 5 6 9]))
axis([0 7 0 1])
xticklabels({'N\N','N\U','N\M','U\U','U\M','M\M'})
% xtickangle(15)
ylabel('Coefficient of Correlation (R)')
fileName=['D:\CorticalNeuronDipole\Paper\Figures\AxonCrossCorrBoxPlot.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
%%
figure
SynapData=[];
for i=0:4
    SynapData=[SynapData e(a.*2+f==i)];
end
% bar(mean(SynapData(:,[1 2 4])))
% hold on
sem=std(SynapData(:,[1 2 4]))./10;
% er = errorbar([1 2 3],mean(SynapData(:,[1 2 4])),sem);    
% er.Color = [0 0 0];                            
% er.LineStyle = 'none'; 
boxplot(SynapData(:,[1 2 4]))
xticklabels({'SA \ SA','SA \ CI','CI \ CI'})
axis([0 4 0 1])
% xtickangle(15)
ylabel('Coefficient of Correlation (R)')
fileName=['D:\CorticalNeuronDipole\Paper\Figures\ActivationTypeCrossCorrBoxPlot.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
%%
figure
plot(linspace(0,10,length(VTraces{2,1,17})),VTraces{1,3,14},'LineWidth',2)
hold on
plot(linspace(0,10,length(VTraces{2,2,17})),VTraces{2,3,14},'LineWidth',2)
legend('Synaptic Activation','Current Injection')
% fileName=['D:\CorticalNeuronDipole\Paper\Figures\ActiationtypeCrossCorrTraces.pdf'];
%     print(gcf,fileName,'-bestfit','-dpdf');