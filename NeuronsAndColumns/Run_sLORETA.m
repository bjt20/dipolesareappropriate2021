%% Old Method with weird names and only layer2/3 PC neuron
% % % files={'IsotropicNeuronVoltageSampledAtRecLocs.mat','IsotropicNeuron_Time2VoltageSampledAtRecLocs.mat',...
% % %     'IsotropicColumnVoltageSampledAtRecLocs.mat','IsotropicDipoleVoltageSampledAtRecLocs.mat',...
% % %     'AnsotropicNeuronVoltageSampledAtRecLocs.mat','AnsotropicNeuron_Time2VoltageSampledAtRecLocs.mat',...
% % %     'AnisotropicColumnVoltageSampledAtRecLocs.mat','AnisotropicDipoleVoltageSampledAtRecLocs.mat',...
% % %     'IsotropicNeuron_WithAxon_81_VoltageSampledAtRecLocs.mat','IsotropicNeuron_WithAxon_106_VoltageSampledAtRecLocs.mat',...
% % %     'IsotropicNeuron_WithAxon_128_VoltageSampledAtRecLocs.mat','AnisotropicNeuron_WithAxon_81_VoltageSampledAtRecLocs.mat',...
% % %     'AnisotropicNeuron_WithAxon_106_VoltageSampledAtRecLocs.mat','AnisotropicNeuron_WithAxon_128_VoltageSampledAtRecLocs.mat'};
% % % Outnames={'IsotropicNeuron','IsotropicNeuron_Time2',...
% % %     'IsotropicColumn','IsotropicDipole',...
% % %     'AnsotropicNeuron','AnsotropicNeuron_Time2',...
% % %     'AnisotropicColumn','AnisotropicDipole',...
% % %     'IsotropicNeuron_withAxon_81','IsotropicNeuron_withAxon_106',...
% % %     'IsotropicNeuron_withAxon_128','AnisotropicNeuron_withAxon_81',...
% % %     'AnisotropicNeuron_withAxon_106','AnisotropicNeuron_withAxon_128'};
% % % iso=[1 2 3 4 9 10 11];
% % %
% % % for j=1:14
% % %     rng(1)
% % %     load(['ForwardRepeatedTrials\' files{j}])
% % %     phis=scirunfield.field;
% % %     for i=1:50
% % %         if ~isempty(intersect(iso,j))
% % %             load(['LeadFields\LeadFieldIso' num2str(i) '.mat'])
% % %         else
% % %             load(['LeadFields\LeadFieldAniso' num2str(i) '.mat'])
% % %         end
% % %         phi=phis(((i-1)*21+1):i*21);
% % %         phi=phi+max(phi).*0.631.*(rand(1,21)-0.5);
% % %         warning('off','MATLAB:nearlySingularMatrix')
% % %
% % %         sLoretaFtn(LeadField,phi',['SourceLocalizationsRepeatedTrials\' Outnames{j} '_' num2str(i) '_5dB.mat'])
% % %     end
% % % end
%% All Neuron Method
% files={'IsotropicNeuronVoltageSampledAtRecLocs.mat','IsotropicNeuron_Time2VoltageSampledAtRecLocs.mat',...
%     'IsotropicColumnVoltageSampledAtRecLocs.mat','IsotropicDipoleVoltageSampledAtRecLocs.mat',...
%     'AnsotropicNeuronVoltageSampledAtRecLocs.mat','AnsotropicNeuron_Time2VoltageSampledAtRecLocs.mat',...
%     'AnisotropicColumnVoltageSampledAtRecLocs.mat','AnisotropicDipoleVoltageSampledAtRecLocs.mat',...
%     'IsotropicNeuron_WithAxon_81_VoltageSampledAtRecLocs.mat','IsotropicNeuron_WithAxon_106_VoltageSampledAtRecLocs.mat',...
%     'IsotropicNeuron_WithAxon_128_VoltageSampledAtRecLocs.mat','AnisotropicNeuron_WithAxon_81_VoltageSampledAtRecLocs.mat',...
%     'AnisotropicNeuron_WithAxon_106_VoltageSampledAtRecLocs.mat','AnisotropicNeuron_WithAxon_128_VoltageSampledAtRecLocs.mat'};
% Outnames={'IsotropicNeuron','IsotropicNeuron_Time2',...
%     'IsotropicColumn','IsotropicDipole',...
%     'AnsotropicNeuron','AnsotropicNeuron_Time2',...
%     'AnisotropicColumn','AnisotropicDipole',...
%     'IsotropicNeuron_withAxon_81','IsotropicNeuron_withAxon_106',...
%     'IsotropicNeuron_withAxon_128','AnisotropicNeuron_withAxon_81',...
%     'AnisotropicNeuron_withAxon_106','AnisotropicNeuron_withAxon_128'};
% iso=[1 2 3 4 9 10 11];
axontype={'NoAxon','Unmyel','Myel'};
medium={'Isotropic','Anisotropic'};
Noise={'','_20db','_5db'};
NoiseMult=[0 0.1 0.5623];
StimType = {'CurrentInj','SynapAct'};

for k=1%1:2 % iso aniso
    if k==1
        load(['LeadFields\LeadFieldIso' num2str(i) '.mat'])
    else
        load(['LeadFields\LeadFieldAniso' num2str(i) '.mat'])
    end
    for g =1%:2
        for q=1%:3
            for p=2%1:3
                for j=11%1:25 % cell number
                    rng(1)
                    %         load(['ForwardRepeatedTrials\' files{j}])
%                     load(['AllNeurons\' StimType{g} 'SLoretaVoltages\' axontype{p} '\Neuron_' medium{k} num2str(j) '.mat'])
                    load(['AllNeurons\' StimType{g} 'SLoretaVoltages\' axontype{p} '\Neuron_' medium{k} num2str(j) '_3.mat'])
                    phis=scirunfield.field;
                    for i=1:10 % sensor config
                        phi=phis(((i-1)*21+1):i*21);
                        phi=phi+max(phi).*NoiseMult(q).*(rand(1,21)-0.5); % Add noise
                        %                 phi=phi+max(phi).*0.1.*(rand(1,21)-0.5);
                        warning('off','MATLAB:nearlySingularMatrix')
                        warning('off','MATLAB:singularMatrix')
                        sLoretaFtn(LeadField,phi',['AllNeurons\' StimType{g} 'Localizations\' axontype{p} '\Neuron_' medium{k} num2str(j) '_' num2str(i) '_Regularized' Noise{q} '_LowerMesh.mat'])
                        
                    end
                end
            end
        end
    end
end