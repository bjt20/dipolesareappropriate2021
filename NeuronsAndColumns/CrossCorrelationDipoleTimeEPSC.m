%% Initialize
radii=[0.05 0.1 0.2 0.3 0.4 0.5 1 2 3 4 5 6 7 8 9 10 15 20];
dipolyness=zeros(25,length(radii)-1);
posz=zeros(25,length(radii)-1);
directions=[];
moment=zeros(25,length(radii)-1);
momentPosz=zeros(25,length(radii)-1);
%% Fits
Folder = {'SynapAct','CurrentInj'};
AxonName = {'maxH_rax1','umaxH','maxH'};
activationName = {'synapact','currinjection'};
Axons={'NoAxon','Unmyel','Myel'};
VTraces={};
saveMoments={};
saveDipoles={};
saveVoltages={};
tic
for k = 1:2 % activation type
    for j = 1:3 % axon types
        load(['AllNeurons\' Folder{k} '\' AxonName{j} '_' activationName{k} '.mat'])
        for i=16:20% L5 number
            if j==1
                radii=[0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 10 20]+max(data_all{i}.coordinates(:,2))./1000;
            else
                radii=[0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 10 20]+max(data_all{i}.coordinates(:,3))./1000;
            end
            load(['sphereLocs\SpherePointsMultiTrialHigherRes_' activationName{k} '_' AxonName{j} '_' num2str(i) '.mat']);
            locsOrig=scirunfield.node;
            load(['AllNeurons\' Folder{k} 'SphereVoltages\DipolynessAndMomentAll_AllDir_' Axons{j} '_EPSC.mat'])
            for index = 1:length(radii)
                % Find timepoints we care about
                if j == 1
                    rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 10 0]).^2,2));
                else
                    rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 0 10]).^2,2));
                end
                current=(data_all{i}.i_mem_matrix.*data_all{i}.areas');
                current(:,1)=current(:,1)-data_all{i}.i_mem_matrix*data_all{i}.areas;
                current=current.*1e-2;%nA
                
                voltageTrace=current*rinv;
                voltageTrace = [voltageTrace.*0;voltageTrace];
                where = find(max(abs(voltageTrace))==abs(voltageTrace));
                current = [current.*0;current];
                
                which=round(radii(index).*100)==round(sqrt(sum(locsOrig.^2)).*100);
                locs=locsOrig(:,which);
                
                % Neuron Voltages in Space
                current = current((where-200):(where+200),:);
                coords = data_all{i}.coordinates./1000;
                data = zeros(401,length(locs));
                for q = 1:length(locs)
                    rinv = 1./sqrt(sum((coords-locs(:,q)').^2,2));
                    data(:,q)=data(:,q)+current*rinv./(4.*pi);
                end
                % Dipole Voltages in Space
                radius=radii(index);% radius of sphere
                [theta,phi]=ind2sub([360 360],directions(i,10));

                direction=[cosd(theta).*cosd(phi) sind(theta).*cosd(phi) sind(phi)];
                angle=sum(direction.*locs',2)./(sqrt(sum(locs.^2))'.*sqrt(sum(direction.^2)));
                dipole=angle./(4.*pi.*radius'.^2);
                dipole=dipole'-mean(dipole);
                Xcoor = zeros(1,401);
                Moments = zeros(1,401);
                for t=1:401
                    if sum(data(t,:))~=0
                        Xcoor(t)=sum((data(t,:)-mean(data(t,:))).*dipole)./(sqrt(sum(dipole.^2)).*sqrt(sum((data(t,:)-mean(data(t,:))).^2)));
%                         Xcoor(t)=corr(data(t,:)',dipole');
                        Moments(t)=dipole'\data(t,:)';
                    end
                end
                saveMoments{k,j,i,index}=Moments;
                saveDipoles{k,j,i,index}=Xcoor;
                if j == 1
                    rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 radii(index) 0]).^2,2));
                else
                    rinv=1./sqrt(sum((data_all{i}.coordinates./1000-[0 0 radii(index)]).^2,2));
                end
                current=(data_all{i}.i_mem_matrix.*data_all{i}.areas');
                current(:,1)=current(:,1)-data_all{i}.i_mem_matrix*data_all{i}.areas;
                current=current.*1e-2;%nA
                current = [current.*0;current];
                voltageTrace=current((where-200):(where+200),:)*rinv;
                saveVoltages{k,j,i,index}=voltageTrace;
            end
        end
    end
end
toc
%%
% % % % % figure
% % % % % for i=1:25
% % % % %     plot(linspace(-5,5,401),-saveMoments{1,1,i})
% % % % %     hold on
% % % % % end
% % % % % figure
% % % % % for i=1:25
% % % % %     plot(linspace(-5,5,401),saveMoments{1,2,i})
% % % % %     hold on
% % % % % end
% % % % % figure
% % % % % for i=1:25
% % % % %     plot(linspace(-5,5,401),saveMoments{1,3,i})
% % % % %     hold on
% % % % % end
% % % % % figure
% % % % % for i=1:25
% % % % %     plot(linspace(-5,5,401),-saveMoments{2,1,i})
% % % % %     hold on
% % % % % end
% % % % % figure
% % % % % for i=1:25
% % % % %     plot(linspace(-5,5,401),saveMoments{2,2,i})
% % % % %     hold on
% % % % % end
% % % % % figure
% % % % % for i=1:25
% % % % %     plot(linspace(-5,5,401),saveMoments{2,3,i})
% % % % %     hold on
% % % % % end
save AllNeurons\DipoleMomentsAndDipolynessEPSCBestDirection_l5.mat saveMoments saveDipoles saveVoltages
%% Good Rep by dipole
figure
i=12;
%     mid=(max(-saveMoments{1,1,i})-min(-saveMoments{1,1,i}))./2+min(-saveMoments{1,1,i});
plot(linspace(-5,5,401),-saveMoments{1,1,i,16})
hold on
plot(linspace(-5,5,401),saveMoments{1,2,i,16})
plot(linspace(-5,5,401),saveMoments{1,3,i,16})
fileName=['D:\CorticalNeuronDipole\Paper\Figures\Neuron' num2str(i) 'DipoleMomentTraces.pdf'];
print(gcf,fileName,'-bestfit','-dpdf');
figure
plot(linspace(-5,5,401),-saveDipoles{1,1,i,16})
hold on
plot(linspace(-5,5,401),saveDipoles{1,2,i,16})
plot(linspace(-5,5,401),saveDipoles{1,3,i,16})
%     plot(linspace(-5,5,401),saveMoments{1,3,i})

fileName=['D:\CorticalNeuronDipole\Paper\Figures\Neuron' num2str(i) 'DipoleCorrTraces.pdf'];
print(gcf,fileName,'-bestfit','-dpdf');
%% Min and Max dipole moments
figure
mins=zeros(5,5);
maxes=zeros(5,5);
for i=1:5
    for j = 1:5
        mins(i,j)=min(-saveMoments{1,1,(i-1).*5+j,16});
        maxes(i,j)=max(-saveMoments{1,1,(i-1).*5+j,16});
%         mins(i,j)=min(saveMoments{1,3,(i-1).*5+j,16});
%         maxes(i,j)=max(saveMoments{1,3,(i-1).*5+j,16});
    end
end
boxplot(mins')
axis([0 6 -1.2 0])
% fileName=['D:\CorticalNeuronDipole\Paper\Figures\MinDipoleMomentMyel.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf');

figure
boxplot(maxes')
axis([0 6 0 3.1])
% fileName=['D:\CorticalNeuronDipole\Paper\Figures\MaxDipoleMomentMyel.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf');
%%
figure
hAx=axes
% axis off
hAx.XScale='log';
risingMetric = zeros(5,5);
for i=1:5
    
    data=zeros(5,length(radii));
    for q=1:5
        for j=1:length(radii)
            data(q,j)=(-saveDipoles{1,1,(i-1)*5+q,j}(201));
        end
        
        %     plot(linspace(-5,5,401),-saveDipoles{1,1,i})
        %     plot(linspace(-5,5,401),saveMoments{1,3,i})
        target = (data(q,:).^2)'./(data(q,end).^2)'.*0.99;
        [x,y]=intersections(radii,0.9.*ones(1,length(target(:))),radii,(data(q,:))'./(data(q,end))')
%         figure(3)
%         hold on
%         plot(radii,data(q,:)'./data(q,end))
        if isempty(x)
            risingMetric(i,q) = 0.05;
        else
            risingMetric(i,q) = min(x);
        end
    end
%     figure(1)
    hold on
    if i==1
%                 plot(radii,data,'b-')
        errorbar(radii,mean(abs(data)),std(abs(data))./sqrt(5),'b-')
    elseif i==2
%                 plot(radii,data,'r-')
        errorbar(radii,mean(abs(data)),std(abs(data))./sqrt(5),'r-')
    elseif i==3
%                 plot(radii,data,'g-')
        errorbar(radii,mean(abs(data)),std(abs(data))./sqrt(5),'g-')
    elseif i==4
%                 plot(radii,data,'m-')
        errorbar(radii,mean(abs(data)),std(abs(data))./sqrt(5),'m-')
    else
%                 plot(radii,data,'c-')
        errorbar(radii,mean(abs(data)),std(abs(data))./sqrt(5),'c-')
    end
    
    hold on
    axis([0.1 10 0 1])
    axis square
    
%     figure(2)
%     hold on
%     plot(diff(data'))
end
fileName=['D:\CorticalNeuronDipole\Paper\Figures\DipoleCorrelationOverSpace_LogScale.pdf'];
%     print(gcf,fileName,'-bestfit','-dpdf');
%%
% figure
SSMetrics = zeros(3,5,5);
for p=1:3
    for i=1:5
        
        data=zeros(5,length(radii));
        for q=1:5
            for j=1:length(radii)
                data(q,j)=abs(saveDipoles{1,p,(i-1)*5+q,j}(201));
            end
            
            %     plot(linspace(-5,5,401),-saveDipoles{1,1,i})
            %     plot(linspace(-5,5,401),saveMoments{1,3,i})
            SSMetrics(p,i,q) = data(q,end);
            
        end
        figure(1)
        hold on
        if i==1
            plot(radii,data,'b-')
            %         errorbar(radii,mean(data),std(data)./sqrt(5),'b-')
        elseif i==2
            plot(radii,data,'r-')
            %         errorbar(radii,mean(data),std(data)./sqrt(5),'r-')
        elseif i==3
            plot(radii,data,'g-')
            %         errorbar(radii,mean(data),std(data)./sqrt(5),'g-')
        elseif i==4
            plot(radii,data,'m-')
            %         errorbar(radii,mean(data),std(data)./sqrt(5),'m-')
        else
            plot(radii,data,'c-')
            %         errorbar(radii,mean(data),std(data)./sqrt(5),'c-')
        end
        
        hold on
        %     axis([0 10 0 1])
        axis square
        
        figure(2)
        hold on
        plot(diff(data'))
    end
end
%%
figure
boxplot(risingMetric')
axis([0 6 0.05 0.7])
ylabel('Recording Distance 90% to Steady State (mm)')
fileName=['D:\CorticalNeuronDipole\Paper\Figures\DipoleCorrelationRiseRateOverSpace.pdf'];
print(gcf,fileName,'-bestfit','-dpdf');

%%
figure
boxplot(squeeze(SSMetrics(1,:,:))')
% boxplot(squeeze(SSMetrics(2,:,:))')
% boxplot(squeeze(SSMetrics(3,:,:))')
axis([0 6 0 1])
ylabel('Steady State Dipole Correlation (R)')
fileName=['D:\CorticalNeuronDipole\Paper\Figures\DipoleCorrelationSteadyState.pdf'];
print(gcf,fileName,'-bestfit','-dpdf');
%% Naes et al figure comp
figure
% figure(1)
time=201;
color=parula(5);
load AllNeurons\DipoleMomentsAndDipolynessAPBestDirection_l5.mat
% load AllNeurons\DipoleMomentsAndDipolynessEPSCBestDirection_l5.mat

% load(['AllNeurons\SynapticActivation\maxH_synapact.mat'])

% load(['AllNeurons\SynapActSphereVoltages\DipolynessAndMomentAll_AllDir_Myel_EPSC.mat'])
load(['AllNeurons\SynapActSphereVoltages\DipolynessAndMomentAll_AllDir_Myel.mat'])

% load(['AllNeurons\SynapActSphereVoltages\DipolynessAndMomentAll_AllDir_NoAxon_EPSC_l5_NoAxon.mat'])
% load(['AllNeurons\SynapActSphereVoltages\DipolynessAndMomentAll_AllDir_NoAxon.mat'])

% load(['AllNeurons\SynapActSphereVoltages\DipolynessAndMomentAll_AllDir_Unmyel_EPSC_l5_Unmyel.mat'])
% load(['AllNeurons\SynapActSphereVoltages\DipolynessAndMomentAll_AllDir_Unmyel.mat'])
for j=16:20
    
    
%     if j==1
%         radii=[0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 10 20]+max(data_all{j}.coordinates(:,2))./1000;
%     else
        radii=[0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 10 20]+max(data_all{j}.coordinates(:,3))./1000;
%     end
    %     figure
    for i=1:10
        a=saveVoltages{1,3,j,i};
        [theta,phi]=ind2sub([360 360],directions(j,i));
        %                 theta=90;
        %                 if j==1 % No Axon
        %                     phi=180;
        %                 else % With Axon
        %                     phi=90;
        %                 end
        direction=[cosd(theta).*cosd(phi) sind(theta).*cosd(phi) sind(phi)];
        angle=sum(direction.*[0 radii(i) 0],2)./(sqrt(sum([0 radii(i) 0].^2))'.*sqrt(sum(direction.^2)))
% angle=sum(direction.*[0 0 radii(i)],2)./(sqrt(sum([0 0 radii(i)].^2))'.*sqrt(sum(direction.^2)));
%         b=saveMoments{1,1,j,i}.*angle./(radii(i).^2);
        b=saveMoments{1,3,j,i}./(radii(i).^2);
        %     plot((b-a')./(b),color(i,:))
        %     hold on
        c(i)=abs((abs(b(time))-abs(a(time)))./b(time));
        
        %     pause
    end
    % figure(1)
    f=semilogx([0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 10 20],abs(c).*100,'LineWidth',2);
    f.Color=color(j-15,:);
    hold on
end
% loglog([0.05 20],[10 10],'k-')
axis([0.05 10 0 100])
xlabel('Distance from apical dendrite (mm)','FontSize',16)
ylabel('% Error in Voltage','FontSize',16)
title('Error in Voltage: Synaptic Activity','FontSize',20)
print -dpng Paper\Figures\ErrorVoltageAP_Myel_zoomed.png
%%
for i=1:25
    figure
    hold on
    plot(linspace(-5,5,401),-saveMoments{1,1,i,18},'Linewidth',2)
    plot(linspace(-5,5,401),saveMoments{1,2,i,18},'Linewidth',2)
    plot(linspace(-5,5,401),saveMoments{1,3,i,18},'Linewidth',2)
    xlabel('Time (ms)','FontSize',16)
    ylabel('Dipole Moment (pA-m)','FontSize',16)
    axis([-5 5 -1.5 3.5])
    fileName=['Paper/Figures/Moments_' num2str(i) '.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
end
%%
for i=1:25
    figure
    hold on
    plot(linspace(-5,5,401),-saveDipoles{1,1,i,18},'Linewidth',2)
    plot(linspace(-5,5,401),saveDipoles{1,2,i,18},'Linewidth',2)
    plot(linspace(-5,5,401),saveDipoles{1,3,i,18},'Linewidth',2)
    xlabel('Time (ms)','FontSize',16)
    ylabel('Correlation Coefficient (R)','FontSize',16)
    axis([-5 5 -1 1])
    fileName=['Paper/Figures/DipoleCorrelation_' num2str(i) '.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
end
%%
threshold=0.9;
vals=[];
for j=1:3
    for i=1:25
        data=saveDipoles{1,j,i,18};
        vals(j,i)=length(data(abs(data)>0.9))./401;
    end
end
figure
boxplot(reshape(vals(1,:),[5 5]))