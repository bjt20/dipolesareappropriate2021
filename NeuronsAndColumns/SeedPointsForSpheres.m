Folder = {'SynapticActivation','CurrentInj'};
AxonName = {'maxH_rax1','umaxH','maxH'};
activationName = {'synapact','currinjection'};
VTraces={};
saveMoments={};
saveDipoles={};
saveVoltages={};
tic
rng(1)
for k = 1:2 % activation type
    for j = 1:3 % axon types
        load(['AllNeurons\' Folder{k} '\' AxonName{j} '_' activationName{k} '.mat'])
        for i=1:25 % Axon number
            if j==1
                radii=[0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 10 20]+max(data_all{i}.coordinates(:,2))./1000;
            else
                radii=[0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 10 20]+max(data_all{i}.coordinates(:,3))./1000;
            end
            m=1;
            n=100;
            trials=50;
            saveLocs=[];
            for r=radii
                theta =rand(5000,1).*360;
                phi=rand(5000,1).*360;
                Locs=[r.*cos(theta).*cos(phi) r.*cos(theta).*sin(phi) r.*sin(theta)];
                saveLocs=[saveLocs;Locs];
            end
            scirunfield.node=saveLocs';
            save(['sphereLocs\SpherePointsMultiTrialHigherRes_' activationName{k} '_' AxonName{j} '_' num2str(i) '.mat'],'scirunfield');
        end
    end
end