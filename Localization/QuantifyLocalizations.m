load Neuron\CorticalCoords.mat
Locs=scirunfield.node;
load ..\ContactLocations.mat
electrodeLocs=tetmesh.node;
ct=1;
for i=1:25
    for j=1:10
        load(['Neuron\Neuron' num2str(i) '_loc_' num2str(j) '.mat']);
        somaLoc=scirunfield.node(:,1);
        load(['Localizations\Neuron_SourceLocalization_' num2str(ct) '.mat']);
        [~,index]=max(scirunmatrix);
        BestLoc=Locs(:,index);
        dist=sqrt(sum((somaLoc-BestLoc).^2));
        distances(i,j)=dist;
        if i==1
            closestElec(j)=min(sqrt(sum((electrodeLocs-somaLoc).^2)));
        end
        ct=ct+1;
    end
end
[~,ind] = sort(closestElec);
ind = ind(end:-1:1);
% errors=std(distances(:,[2 3 4 5 6 7 9 10])')./sqrt(8);
% bar(reshape(mean(distances(:,[2 3 4 5 6 7 9 10]),2),[5,5])')
%% Localization Error of column one time
load Neuron\CorticalCoords.mat
Locs=scirunfield.node;
load ..\ContactLocations.mat
electrodeLocs=tetmesh.node;
Coldistances=[];
times=5951;
for k=1:length(times)
for j=1:10
    load(['Column\Column_' num2str(j) '.mat']);
    somaLoc=scirunfield.node(:,1);
    load(['Localizations\Column_SourceLocalization_' num2str(j) '_' num2str(times(k)) '.mat']);
    [~,index]=max(scirunmatrix);
    BestLoc=Locs(:,index);
    dist=sqrt(sum((somaLoc-BestLoc).^2));
    Coldistances(j,k)=dist;
end
end
figure
for i=1:10
%     figure
    hold on
    plot(Coldistances(i,:),'*')
end
%% Localization Error of column all times
load Neuron\CorticalCoords.mat
Locs=scirunfield.node;
load ..\ContactLocations.mat
electrodeLocs=tetmesh.node;
Coldistances=[];
times=[44 598 1146 1693 2227 2970 3850 4361 4863 5635 6431 6967 7499 8072 8900 9649 102 2287 4415 6008];
for k=1:length(times)
for j=1:10
    load(['Column\Column_' num2str(j) '.mat']);
    somaLoc=scirunfield.node(:,1);
    load(['Localizations\Column_SourceLocalization_' num2str(j) '_' num2str(times(k)) '.mat']);
    [~,index]=max(scirunmatrix);
    BestLoc=Locs(:,index);
    dist=sqrt(sum((somaLoc-BestLoc).^2));
    Coldistances(j,k)=dist;
end
end
figure
load Xcoor.mat
color=parula(10);
for i=1:10
%     figure
    hold on
    f=plot(abs(Xcoor(times)),Coldistances(i,:),'*');
    f.Color=color(i,:);
end
xlabel('Coefficient of Correlation (R)','FontSize',16)
ylabel('Localization Error (mm)','FontSize',16)
title('Localization Error Over Time: All Directions','FontSize',18)
% print -dpng Figures/LocalizationErrorColumnTime_AllDirections.png
fileName='Figures\LocalizationErrorColumnTime_AllDirections.pdf';
print(gcf,fileName,'-bestfit','-dpdf')
%% Localization error of column only normal lead field
load ElementLocations.mat
Locs=scirunfield.field+[0;0;68];
load ..\ContactLocations.mat
electrodeLocs=tetmesh.node;
times=[44 598 1146 1693 2227 2970 3850 4361 4863 5635 6431 6967 7499 8072 8900 9649 102 2287 4415 6008];
for k=1:length(times)
for j=1:10
    load(['Column\Column_' num2str(j) '.mat']);
    somaLoc=scirunfield.node(:,1);
    load(['Localizations\Column_SourceLocalization_NormalLocalization_' num2str(j) '_' num2str(times(k)) '.mat']);
    [~,index]=max(scirunmatrix);
    BestLoc=Locs(:,index);
    dist=sqrt(sum((somaLoc-BestLoc).^2));
    Coldistances(j,k)=dist;
end
end
figure
load Xcoor.mat
for i=1:10
%     figure
    hold on
    f=plot(abs(Xcoor(times)),Coldistances(i,:),'*');
    f.Color=color(i,:);
% boxplot(Coldistances)
end
xlabel('Coefficient of Correlation (R)','FontSize',16)
ylabel('Localization Error (mm)','FontSize',16)
title('Localization Error Over Time: Only Normals','FontSize',18)
% print -dpng Figures/LocalizationErrorColumnTime_Normals.png
fileName='Figures\LocalizationErrorColumnTime_Normals.pdf';
print(gcf,fileName,'-bestfit','-dpdf')
%%
for j=1:10
    load(['Dipole\Dipole_' num2str(j) '.mat']);
    somaLoc=scirunfield.node(:,1);
    load(['Localizations\Dipole_SourceLocalization_' num2str(j) '.mat']);
    [~,index]=max(scirunmatrix);
    BestLoc=Locs(:,index);
    dist=sqrt(sum((somaLoc-BestLoc).^2));
    Dipoledistances(j)=dist;
end
%%
ct=1;
for i=1:25
    for j=1:10
        load(['Neuron\Neuron' num2str(i) '_loc_' num2str(j) '.mat']);
        somaLoc=scirunfield.node(:,1);
        load(['Localizations\Neuron_SourceLocalization_' num2str(ct) '_Unmyel.mat']);
        [~,index]=max(scirunmatrix);
        BestLoc=Locs(:,index);
        dist=sqrt(sum((somaLoc-BestLoc).^2));
        distancesUnmyel(i,j)=dist;
        
        ct=ct+1;
    end
end
%%
ct=1;
for i=1:25
    for j=1:10
        load(['Neuron\Neuron' num2str(i) '_loc_' num2str(j) '.mat']);
        somaLoc=scirunfield.node(:,1);
        load(['Localizations\Neuron_SourceLocalization_' num2str(ct) '_Myel.mat']);
        [~,index]=max(scirunmatrix);
        BestLoc=Locs(:,index);
        dist=sqrt(sum((somaLoc-BestLoc).^2));
        distancesMyel(i,j)=dist;
        
        ct=ct+1;
    end
end
%%
figure
% hold on
col={'r','g','b','m','k'};
mark={'o','s','*','x','d'};
ct=1;
% semilogy(closestElec,Dipoledistances,'r*')
plot(closestElec(ind),Dipoledistances(ind),'ro')
hold on
% semilogy(closestElec,Coldistances,'b*')
% plot(closestElec(ind),Coldistances(ind),'bo')
for i=1:10
    uniquedata=unique(round(distances(:,i).*10));
    for j=1:length(uniquedata)
%         semilogy(closestElec,distances(ct,:),'k*')
        plot(closestElec(i),uniquedata(j)./10,'ko','MarkerSize',length(find(round(distances(:,i).*10)==uniquedata(j)))/3+4)
        hold on
        ct=ct+1;
    end
end
ct=1;
for i=1:10
    uniquedata=unique(distancesUnmyel(:,i));
    for j=1:length(uniquedata)
%         semilogy(closestElec,distances(ct,:),'k*')
        plot(closestElec(i),distancesUnmyel(ct,(ind)),'mo')
        hold on
        ct=ct+1;
    end
end
ct=1;
for i=1:5
    for j=1:5
%         semilogy(closestElec,distances(ct,:),'k*')
        plot(closestElec(ind),distancesMyel(ct,(ind)),'go')
        hold on
        ct=ct+1;
    end
end
plot(closestElec(ind),Dipoledistances(ind),'ro')
plot(closestElec(ind),Coldistances(ind),'bo')

xlabel('Distance of Source to Nearest Electrode (mm)')
ylabel('Localization Error (mm)')
legend('Dipole','Neuron')
% axis([0 50 0 10])
% fileName=['D:\CorticalNeuronDipole\Paper\Figures\RealHeadLocalizationErrorNoNoise_Updated.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf');
