load('LeadFieldCorticalSurface/LeadMatrixCorticalSurface.mat')
Noise={'_20db','_5db'};
NoiseMult=[0.1 0.5623];
numContacts = length(LeadField(:,1));
numElem = length(LeadField(1,:))/3;
t=5951;%[44 598 1146 1693 2227 2970 3850 4361 4863 5635 6431 6967 7499 8072 8900 9649 102 2287 4415 6008];
%%
for k=t
    for ct=1:10
        name =['RealSources\VoltagesAtRecColumn\Column_VoltagesAtRec_' num2str(ct) '_' num2str(k) '.mat'];
%         name =['RealSources\VoltagesAtRec\Column_VoltagesAtRec_' num2str(ct) '.mat'];
        if exist(name)
            load(name);
            phi = scirunmatrix;
            phi=phi(2:end,1);
            %         phi=phi+(max(phi).*NoiseMult(k).*(rand(1,length(phi))-0.5).*2)';
            warning('off','MATLAB:nearlySingularMatrix')
            
            %% Create H matrix
            RecordingSites = numContacts; % Number of recording sites
            one = ones(RecordingSites,1); % Ones vector
            H = eye(RecordingSites)-one*one'./RecordingSites; % Centering Matrix
            
            %% Regularize Lead-Field and Recordings
            phi = H*phi;%./norm(phi);
            K = H*LeadField;%./(Power(:)');
            
            [U,s,V] = csvd(K);
            Alp = discrep4(U,s,V,phi,norm(phi));
            alpha=Alp.^2;%10^order;% constant regulatization parameter >=0
            %% Make T matrix = K'H[HKK'H+aH]+
            T=K'*pinv(K*K'+alpha.*H); % make T matrix #rec x #dipoles
            
            %% Calculate Jhat
            Jhat = T*phi;
            %% Loop through each element to find Localization inference map
            LIM=zeros(1,length(K)/3);
            for i=1:length(K)/3
                index=(3*(i-1)+1);
                dipole = Jhat(index:index+2,1);
                covar=T(index:index+2,:)*K(:,index:index+2);
                LIM(i)=dipole'/covar*dipole;
            end
            scirunmatrix=LIM';
            %         save(['RealSources\Localizations\Dipole_SourceLocalization_' num2str(ct) Noise{k} '.mat'],'scirunmatrix')
            save(['RealSources\Localizations\Column_SourceLocalization_' num2str(ct) '_' num2str(k) '.mat'],'scirunmatrix')
%             save(['RealSources\Localizations\Column_SourceLocalization_' num2str(ct) '.mat'],'scirunmatrix')
            
            %% Loop through each element to find Localization inference map normals known orientation
%             LIM=zeros(1,length(K));
%             for i=1:length(K)
%                 dipole = Jhat(i);
%                 covar=T(i,:)*K(:,i);
%                 LIM(i)=dipole.^2./covar;
%             end
%             scirunmatrix=LIM';
%             %         save(['RealSources\Localizations\Dipole_SourceLocalization_' num2str(ct) Noise{k} '.mat'],'scirunmatrix')
%             save(['RealSources\Localizations\Column_SourceLocalization_NormalLocalization_' num2str(ct) '_' num2str(k) '.mat'],'scirunmatrix')
        end
    end
end