This folder contains all the code needed to run the patient specific source localization analysis for Thio et al. 2021 (IDEAL CURRENT DIPOLES ARE APPROPRIATE SOURCE REPRESENTATIONS OF NEURONS FOR INTRACRANIAL RECORDINGS).

PatientForward_* simulates the lead field for neuron and cortical column models

sLoreta* runs sLoreta for the models

Master.slurm and WriteAutomationFilesRealSources.m are for parallelizing the code on a compute cluster.