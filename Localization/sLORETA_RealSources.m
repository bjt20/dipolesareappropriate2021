% clear
% clc
% mesh=[3 4 5 9];
% which=[323456 2765841 2860017 2854291];
% % load(['Sensitivity/LeadMatrixReciprocityMesh_' num2str(mesh(4)) '.mat'])
load('LeadFieldCorticalSurface/LeadMatrixCorticalSurface.mat')

numContacts = length(LeadField(:,1));
numElem = length(LeadField(1,:))/3;
AxonType = {'','_Unmyel','_Myel'};
StimType = {'_SA',''};
Noise={'_20db','_5db'};
NoiseMult=[0.1 0.5623];
%%
% ct=1;
for k=1%:2
    for p=1:2
        for j=1:3
            for ct=1:250%j=1:2%:25
                %     for k=1:10
                %         load(['RealSources\Neuron\Neuron' num2str(j) '_VoltagesAtRec_' num2str(k) '.mat']);
                load(['RealSources\VoltagesAtRec\Neuron_VoltagesAtRec_' num2str(ct) AxonType{j} StimType{p} '.mat']);
                phi = scirunmatrix;
                phi=phi(2:end,1);
%                 phi=phi+(max(abs(phi)).*NoiseMult(k).*(rand(1,length(phi))-0.5).*2)';
                warning('off','MATLAB:nearlySingularMatrix')
                
                %% Create H matrix
                RecordingSites = numContacts; % Number of recording sites
                one = ones(RecordingSites,1); % Ones vector
                H = eye(RecordingSites)-one*one'./RecordingSites; % Centering Matrix
                
                %% Regularize Lead-Field and Recordings
                phi = H*phi;%./norm(phi);
                K = H*LeadField;%./(Power(:)');
                
                [U,s,V] = csvd(K);
                Alp = discrep4(U,s,V,phi,norm(phi));
                alpha=Alp.^2;%10^order;% constant regulatization parameter >=0
                %% Make T matrix = K'H[HKK'H+aH]+
                T=K'*pinv(K*K'+alpha.*H); % make T matrix #rec x #dipoles
                
                %% Calculate Jhat
                Jhat = T*phi;
                %% Loop through each element to find Localization inference map
                LIM=zeros(1,length(K)/3);
                for i=1:length(K)/3
                    index=(3*(i-1)+1);
                    dipole = Jhat(index:index+2,1);
                    covar=T(index:index+2,:)*K(:,index:index+2);
                    LIM(i)=dipole'/covar*dipole;
                end
                scirunmatrix=LIM;
                %         Locs(:,find(scirunmatrix==max(scirunmatrix)))
                %%
                %         kept=[];
                %         load ScirunMeshConnectivity.mat
                %         keepGoing=1;
                %         while length(kept)<10000
                %             kept=find(LIM>0.01/keepGoing*max(abs(LIM)));%Find Nodes to keep 1% of max
                %             connected=cell2mat(connectivity(kept)')';
                %             kept=intersect(connected,kept);%only keep elements which have some other active element next to them
                %         %     if isempty(keep)
                %         %         LIM(kept)=0;
                %         %     end
                %             keepGoing=keepGoing*10;
                %         end
                %         % kept=keep;
                %         mid=median(LIM(kept));
                %         for i=1:length(kept)
                %             neighbors=cell2mat(connectivity(kept(i)'));
                %             maxneighbor=mean(LIM(neighbors));
                %             if maxneighbor<LIM(kept(i))*.7
                %         %         disp('1')
                %                 LIM(kept(i))=0;
                %             end
                %         %     if LIM(kept(i))>(mid*5)
                %         %         disp('2')
                %         %         LIM(kept(i))=0;
                %         %     end
                %         end
                %%
                %         scirunmatrix=zeros(1,length(LIM));
                %         scirunmatrix(kept)=LIM(kept)';
                scirunmatrix=LIM';
                save(['RealSources\Localizations\Neuron_SourceLocalization_' num2str(ct) AxonType{j} StimType{p} '.mat'],'scirunmatrix')
%                 save(['RealSources\Localizations\Neuron_SourceLocalization_' num2str(ct) AxonType{j} StimType{p} Noise{k} '.mat'],'scirunmatrix')
                %         ct=ct+1;
                %     end
            end
        end
    end
end