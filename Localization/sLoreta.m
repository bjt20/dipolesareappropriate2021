%% Load in Lead-Field, Alpha, and recorded potentials
% load('LeadMatrixReciprocity_xyz.mat')
% LeadField = scirunmatrix(:,4:end)';% Load lead field matrix from SCIRun
% numContacts = length(LeadField(:,1))/3;
% numElem = length(LeadField(1,:));
% data=[];
% for j=1:numContacts
%     elem=[];
%     for i=1:numElem
%         elem=[elem LeadField(3*(j-1)+1,i) LeadField(3*(j-1)+2,i) LeadField(3*(j-1)+3,i)];
%     end
%     data=[data;elem];
% end
% LeadField=data;

clear
clc
mesh=[3 4 5 9];
which=[323456 2765841 2860017 2854291];
load(['Sensitivity/LeadMatrixReciprocityMesh_' num2str(mesh(4)) '.mat'])
phiReal = LeadField(:,(which-1)*3+1);
for j=1:4
    for k=1:2
        % load LeadMatrixReorganized5mil_COnverged.mat
        load(['Sensitivity/LeadMatrixReciprocityMesh_' num2str(mesh(k)) '.mat'])
        
        numContacts = length(LeadField(:,1));
        numElem = length(LeadField(1,:))/3;
        
%         discount=find(LeadField>(max(max(abs(LeadField)))*1e-9));
%         discount=unique(ceil(ceil(discount./111)./3));
%         takeOut=[3.*(discount-1)+1;3.*(discount-1)+2;3.*(discount-1)+3];
%         LeadField(:,takeOut)=1e-10;
        % load('SingleDipoleTest.mat')
        % load('SingleDipoleInInsula.mat')
        phi=phiReal(:,j);
        % phi=scirunmatrix(2:end,1);
        warning('off','MATLAB:nearlySingularMatrix')
        
        %% Create H matrix
        RecordingSites = numContacts; % Number of recording sites
        one = ones(RecordingSites,1); % Ones vector
        H = eye(RecordingSites)-one*one'./RecordingSites; % Centering Matrix
        
        %% Regularize Lead-Field and Recordings
        % rng(1)
        % phi=phi+max(phi).*(2.*rand(length(phi),1)-1)./10;
        phi = H*phi;%./norm(phi);
        % a=(unique(ceil(find(max(abs(LeadField))>100)/3)));%perpetrator elem
        % kept=1:numElem;
        % kept(a)=[];
        % Power=sqrt(sum(reshape(LeadField,[3*numContacts,length(LeadField)/3]).^2));
        % Power=[Power;Power;Power];
        % load AreasOfElementsInMesh.mat
        % areas=[scirunfield.field;scirunfield.field;scirunfield.field];
        K = H*LeadField;%./(Power(:)');
        
        [U,s,V] = csvd(K);
        Alp = discrep4(U,s,V,phi,norm(phi)./2);
        alpha=Alp^2;%10^order;% constant regulatization parameter >=0
        
        % normalizer=sqrt(sum((reshape(sqrt(sum(LeadField.^2)),[3,length(LeadField)/3])).^2));
        % normalizer=[normalizer;normalizer;normalizer];
        % K = H*LeadField./(normalizer(:)');
        
        % K=H*LeadField./(max(abs(LeadField)));
        % K(:,[(a-1)*3+1 (a-1)*3+2 (a-1)*3+3]) = [];
        % clear LeadField %Free up memory
        % alpha=0;%max(max(K*K'));
        %% Make T matrix = K'H[HKK'H+aH]+
        T=K'*pinv(K*K'+alpha.*H); % make T matrix #rec x #dipoles
        
        %% Calculate Jhat
        Jhat = T*phi;
        % currDensity=sqrt(sum(reshape(Jhat,[3 length(Jhat)/3]).^2));
        % scirunmatrix=currDensity';
        % save('CurrentDensities.mat','scirunmatrix')
        %% Loop through each element to find Localization inference map
        LIM=zeros(1,length(K)/3);
        for i=1:length(K)/3
            index=(3*(i-1)+1);
            dipole = Jhat(index:index+2,1);
            covar=T(index:index+2,:)*K(:,index:index+2);
            LIM(i)=dipole'/covar*dipole;
        end
        % kept=[];
        % load ScirunMeshConnectivity.mat
        % keepGoing=1;
        % while length(kept)<10000
        %     kept=find(LIM>0.01/keepGoing*max(abs(LIM)));%Find Nodes to keep 1% of max
        %     connected=cell2mat(connectivity(kept)')';
        %     kept=intersect(connected,kept);%only keep elements which have some other active element next to them
        % %     if isempty(keep)
        % %         LIM(kept)=0;
        % %     end
        %     keepGoing=keepGoing*10;
        % end
        % % kept=keep;
        % mid=median(LIM(kept));
        % for i=1:length(kept)
        %     neighbors=cell2mat(connectivity(kept(i)'));
        %     maxneighbor=mean(LIM(neighbors));
        %     if maxneighbor<LIM(kept(i))*.7
        % %         disp('1')
        %         LIM(kept(i))=0;
        %     end
        % %     if LIM(kept(i))>(mid*5)
        % %         disp('2')
        % %         LIM(kept(i))=0;
        % %     end
        % end
        %%
        % scirunmatrix=zeros(1,length(LIM));
        % scirunmatrix(kept)=LIM(kept)';
        scirunmatrix=LIM';
        save(['SLORETA_Solution_SingleDipoleRegularizationDiscrep_Mesh' num2str(mesh(k)) '_Node' num2str(which(j)) '_normPhi2.mat'],'scirunmatrix')
    end
end