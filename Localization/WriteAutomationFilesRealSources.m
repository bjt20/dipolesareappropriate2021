ct=0;
homeDirectory='/hpchome/wmglab/bjt20/sEEGAutomationLeadField/';
AxonType = {'','_Unmyel','_Myel'};
StimType = {'_SA',''};
for k=1:2
    for j=1:3
        for i=1:250
            ct=ct+1;
            fid = fopen(['AutomationScriptsRealSources/SourceLocalization_sEEG_' num2str(ct) '.py'],'w');
            fprintf(fid,'import os\n');
            fprintf(fid,['scirun_load_network("' homeDirectory 'PatientForward_RealSources.srn5")\n']);
                        
            fprintf(fid,['scirun_set_module_state("ReadField:177","Filename","' homeDirectory 'Neuron/Neuron_' num2str(i) AxonType{j} StimType{k} '.mat")\n']);
            fprintf(fid,['scirun_set_module_state("SolveLinearSystem:13","TargetError",1e-8)\n']);
            fprintf(fid,['scirun_set_module_state("WriteMatrix:0","Filename","' homeDirectory 'VoltagesAtRecs/Neuron_VoltagesAtRec_' num2str(i) AxonType{j} StimType{k} '.mat")\n']);
            fprintf(fid,['scirun_save_network("/hpchome/wmglab/bjt20/sEEGAutomationLeadField/NetworksAllNeurons/PatientForward_RealSources' num2str(ct) '.srn5")\n']);
            fclose(fid);
            %     system(['scirun -s MeshSensitivity' num2str(i) '.py'])
        end
    end
end
